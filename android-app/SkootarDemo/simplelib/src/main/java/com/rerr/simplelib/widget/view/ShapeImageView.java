package com.rerr.simplelib.widget.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.lang.ref.SoftReference;

/**
 * Created by Rerr on 6/27/2015.
 * Description
 * -> describe here
 */
abstract public class ShapeImageView extends ImageView{
    private static final String TAG = ShapeImageView.class.getSimpleName();
    /**
     * Keep original drawable
     */
    private Drawable mOriginDrawable;

    public ShapeImageView(Context context) {
        super(context);
    }

    public ShapeImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ShapeImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ShapeImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        mOriginDrawable = drawable;
        if(drawable instanceof TransitionDrawable) {
            TransitionDrawable td = (TransitionDrawable) drawable;

            //Setup ID to layers
            for(int i=0; i<td.getNumberOfLayers(); i++){
                td.setId(i, i);
            }

            //Replace circular drawable to all layer
            for(int i=0; i<td.getNumberOfLayers(); i++){
                SoftReference<Drawable> softDrawable =
                        new SoftReference<Drawable>(createShapeDrawable(td.findDrawableByLayerId(i), 5));
                Drawable c = softDrawable.get();
                td.setDrawableByLayerId(i, c);
            }
            super.setImageDrawable(td);
        }
        else {
            SoftReference<Drawable> softDrawable = new SoftReference<Drawable>(createShapeDrawable(drawable, 5));
            super.setImageDrawable(softDrawable.get());
        }
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        SoftReference<Drawable> softDrawable = new SoftReference<Drawable>(createShapeDrawable(bm, 5));
        super.setImageDrawable(softDrawable.get());
    }

    @Override
    public Drawable getDrawable() {
        return mOriginDrawable;
    }

    @Override
    public void setBackgroundDrawable(Drawable background) {
        SoftReference<Drawable> softDrawable = new SoftReference<Drawable>(createShapeDrawable(background, 5));
        super.setBackgroundDrawable(softDrawable.get());
    }

    @Override
    public void setBackground(Drawable background) {
        SoftReference<Drawable> softDrawable = new SoftReference<Drawable>(createShapeDrawable(background, 5));
        super.setBackground(softDrawable.get());
    }

    @Override
    protected boolean verifyDrawable(Drawable dr) {
        return super.verifyDrawable(dr) || dr instanceof TransitionDrawable;
    }

    abstract protected Drawable createShapeDrawable(Drawable original, int margin);
    abstract protected Drawable createShapeDrawable(Bitmap original, int margin);

}
