package com.rerr.simplelib.widget.layout;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

/**
 * Created by Rerr on 6/17/2015.
 * Description
 * -> describe here
 */
public class DragHelperLayout extends FrameLayout {

    private static final String TAG = "DragHelperLayout";

    /**
     * Enable Drag direction
     */
    public static final int DRAG_ORIENTATION_EN_ALL = 1;
    public static final int DRAG_ORIENTATION_EN_HORIZONTAL = 2;
    public static final int DRAG_ORIENTATION_EN_VERTICAL = 3;
    public static final int DIRECTION_FORWARD = 0;
    public static final int DIRECTION_REVERSE = 1;

    private ViewDragHelper mDragHelper;
    private ViewDragHelper.Callback mDragHelperCallback;
    private View mTargetChildView;
    private int mHorizontalDragRange;
    private int mVerticalDragRange;
    private final Rect mTrackingTargetBound = new Rect();
    private final Rect mTrackingTargetPosition = new Rect();
    private boolean mTrackingTargetVisible = false;
    private int mEdgeTracking = ViewDragHelper.EDGE_TOP;
    private boolean mShouldIntercept = false;

    /**
     * Range to move rate
     */
    private double mRatioMove = 1.0f; //Full movement

    /**
     * View Released Listener
     */
    private OnViewRelease mViewReleaseListener;

    /**
     * Confirm Intercept Touch
     */
    private OnConfirmInterceptTouch mOnConfirmInterceptTouchListener;

    /**
     * Confirm Touch
     */
    private OnConfirmTouch mOnConfirmTouchListener;

    /**
     * Flag enable drag direction
     */
    private int mOrientation = DRAG_ORIENTATION_EN_ALL;
    private int mDirectionMove = DIRECTION_FORWARD;

    /**
     * Flag curent Drag State
     */
    private int mCurrentDragState = ViewDragHelper.STATE_IDLE;

    /**
     * Flag set visible on start or not
     */
    private boolean mVisibleOnStart = false;

    public void setVisibleOnStart(boolean isVisible){
        mVisibleOnStart = isVisible;
    }

    public int getCurrentDragState() {
        return mCurrentDragState;
    }

    /**
     * Set Enable Direction to Draggable
     *
     * @param orientation is direction choose one of {@link #DRAG_ORIENTATION_EN_ALL},
     *                   {@link #DRAG_ORIENTATION_EN_HORIZONTAL}, {@link #DRAG_ORIENTATION_EN_VERTICAL}
     *
     */
    public void setDragOrientation(int orientation, int directionMove) {
        this.mOrientation = orientation;
        this.mDirectionMove = directionMove;
    }

    public void setHorizontalDragRange(int range) {
        this.mHorizontalDragRange = range;
    }

    public void setVerticalDragRange(int range) {
        this.mVerticalDragRange = range;
    }

    public void setTargetChildView(View view){
        mTargetChildView = view;
    }

    public View getTargetChildView(){
        return mTargetChildView;
    }

    public void setEdgeTrackingEnable(int edgeId){
        mEdgeTracking = edgeId;
        mDragHelper.setEdgeTrackingEnabled(edgeId);
    }

    /**
     * Set View Released Listener
     *
     * @param listener is listen when view is released
     *
     */
    public void setViewReleaseListener(OnViewRelease listener) {
        mViewReleaseListener = listener;
    }

    public void setOnConfirmInterceptTouchListener(OnConfirmInterceptTouch listener){
        mOnConfirmInterceptTouchListener = listener;
    }

    public void setOnConfirmTouchListener(OnConfirmTouch listener){
        mOnConfirmTouchListener = listener;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DragHelperLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public DragHelperLayout(Context context) {
        this(context, null, 0);
    }

    public DragHelperLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DragHelperLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        mDragHelperCallback = createDragCallBack();
        mDragHelper = ViewDragHelper.create(this, mDragHelperCallback);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if(isInEditMode())
            return;

        if(mVisibleOnStart) {
            showTargetView(false);
        }
        else
            hideTargetView(false);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        //Layout all child
        super.onLayout(changed, left, top, right, bottom);

        //Layout target child following save state
        if(mTrackingTargetBound.left != mTrackingTargetBound.right &&
                mTrackingTargetBound.top != mTrackingTargetBound.bottom)
        {
            if(changed){
                if(!mTrackingTargetVisible){
                    hideTargetView(false);
                }
                else{
                    showTargetView(false);
                }
            }
            else {
                mTargetChildView.layout(
                        mTrackingTargetBound.left,
                        mTrackingTargetBound.top,
                        mTrackingTargetBound.right,
                        mTrackingTargetBound.bottom
                );
            }
        }

        if(!isInEditMode())
            mTrackingTargetVisible = isTargetViewVisible();
    }

    public void showTargetView(boolean isSmooth){
        int l, t, r, b;
        l = 0;
        t = 0;
        r = l + getWidth();
        b = t + getHeight();
        if(isSmooth) {
            mDragHelper.smoothSlideViewTo(mTargetChildView, l, t);
            postInvalidate();
        }
        else {
            mTargetChildView.layout(l, t, r, b);
            postInvalidate();
        }
        mTrackingTargetBound.set(l, t, r, b);
    }

    public void hideTargetView(boolean isSmooth){
        int l, t, r, b;
        if(mOrientation == DRAG_ORIENTATION_EN_VERTICAL && mDirectionMove == DIRECTION_REVERSE){
            l = 0;
            t = 0 - getHeight();
            r = l + getWidth();
            b = t + getHeight();
        }
        else if(mOrientation == DRAG_ORIENTATION_EN_VERTICAL && mDirectionMove == DIRECTION_FORWARD){
            l = 0;
            t = 0 + getHeight();
            r = l + getWidth();
            b = t + getHeight();
        }
        else if(mOrientation == DRAG_ORIENTATION_EN_HORIZONTAL && mDirectionMove == DIRECTION_REVERSE){
            l = 0 - getWidth();
            t = 0;
            r = l - getWidth();
            b = t + getHeight();
        }
        else{
            l = 0 + getWidth();
            t = 0;
            r = l + getWidth();
            b = t + getHeight();
        }
        if(isSmooth) {
            mDragHelper.smoothSlideViewTo(mTargetChildView, l, t);
            postInvalidate();
        }
        else {
            mTargetChildView.layout(l, t, r, b);
            postInvalidate();
        }
        mTrackingTargetBound.set(l, t, r, b);
    }

    public void slideTargetViewBy(int offsetX, int offsetY, boolean isSmooth){
        int l, t, r, b;
        l = mTargetChildView.getLeft() + offsetX;
        t = mTargetChildView.getTop() + offsetY;
        r = l + mTargetChildView.getWidth();
        b = t + mTargetChildView.getHeight();
        if(isSmooth) {
            mDragHelper.smoothSlideViewTo(mTargetChildView, l, t);
            postInvalidate();
        }
        else {
            mTargetChildView.layout(l, t, r, b);
            postInvalidate();
        }
        mTrackingTargetBound.set(l, t, r, b);
    }

    public void slideTargetViewTo(int left, int top, boolean isSmooth){
        int l, t, r, b;
        l = left;
        t = top;
        r = l + mTargetChildView.getWidth();
        b = t + mTargetChildView.getHeight();
        if(isSmooth) {
            mDragHelper.smoothSlideViewTo(mTargetChildView, l, t);
            postInvalidate();
        }
        else {
            mTargetChildView.layout(l, t, r, b);
            postInvalidate();
        }
        mTrackingTargetBound.set(l, t, r, b);
    }

    public boolean isTargetViewVisible(){
        int l, t, r, b;
        l = 0;
        t = 0;
        r = l + getWidth();
        b = t + getHeight();
        Rect container = new Rect(l, t, r, b);
        Rect target = new Rect();
        mTargetChildView.getHitRect(target);

        return container.intersect(target);
    }

    public boolean isTargetViewFullVisible(){
        int l, t, r, b;
        l = 0;
        t = 0;
        r = l + getWidth();
        b = t + getHeight();
        Rect container = new Rect(l, t, r, b);
        Rect target = new Rect();
        mTargetChildView.getHitRect(target);

        return container.contains(target);
    }

    private ViewDragHelper.Callback createDragCallBack() {
        return new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                if(mTargetChildView == null)
                    return false;

                return mTargetChildView.getId() == child.getId() && pointerId == 0;
            }

            @Override
            public void onEdgeTouched(int edgeFlags, int pointerId) {
                super.onEdgeTouched(edgeFlags, pointerId);
                String edgeName;
                switch (edgeFlags){
                    case ViewDragHelper.EDGE_LEFT:
                        edgeName = "left";
                        break;
                    case ViewDragHelper.EDGE_TOP:
                        edgeName = "top";
                        break;
                    case ViewDragHelper.EDGE_RIGHT:
                        edgeName = "right";
                        break;
                    case ViewDragHelper.EDGE_BOTTOM:
                        edgeName = "bottom";
                        break;
                    case ViewDragHelper.EDGE_ALL:
                        edgeName = "all";
                        break;
                    default:
                        edgeName = null;
                        break;
                }
                Log.d(TAG, "edge " + (edgeName != null ? edgeName : "unidentified") + " is touched");

            }

            @Override
            public void onEdgeDragStarted(int edgeFlags, int pointerId) {
                mDragHelper.captureChildView(mTargetChildView, pointerId);
            }

            @Override
            public int clampViewPositionHorizontal(View child, int left, int dx) {

                if(mOrientation != DRAG_ORIENTATION_EN_ALL && mOrientation != DRAG_ORIENTATION_EN_HORIZONTAL)
                    return 0;

                int leftBound, rightBound, newLeft;
                if(mDirectionMove == DIRECTION_REVERSE){
                    leftBound = getPaddingTop() - getWidth();
                    rightBound = getPaddingTop();
                    newLeft = Math.min(Math.max(left, leftBound), rightBound);
                }
                else{
                    leftBound = getPaddingTop();
                    rightBound = getWidth();
                    newLeft = Math.min(Math.max(left, leftBound), rightBound);
                }

                return newLeft;
            }

            @Override
            public int clampViewPositionVertical(View child, int top, int dy) {

                if(mOrientation != DRAG_ORIENTATION_EN_ALL && mOrientation != DRAG_ORIENTATION_EN_VERTICAL)
                    return 0;

                int topBound, bottomBound, newTop;
                if(mDirectionMove == DIRECTION_REVERSE){
                    topBound = getPaddingTop() - getHeight();
                    bottomBound = getPaddingTop();
                    newTop = Math.min(Math.max(top, topBound), bottomBound);
                }
                else{
                    topBound = getPaddingTop();
                    bottomBound = getHeight();
                    newTop = Math.min(Math.max(top, topBound), bottomBound);
                }

                return newTop;
            }

            @Override
            public int getViewHorizontalDragRange(View child) {
                return mHorizontalDragRange == 0 ? getWidth() : mHorizontalDragRange;
            }

            @Override
            public int getViewVerticalDragRange(View child) {
                return mVerticalDragRange == 0 ? getHeight() : mVerticalDragRange;
            }

            @Override
            public void onViewDragStateChanged(int state) {
                super.onViewDragStateChanged(state);
                mCurrentDragState = state;
            }

            @Override
            public void onViewReleased(View releasedChild, float xvel, float yvel) {
                super.onViewReleased(releasedChild, xvel, yvel);

                if(mOrientation == DRAG_ORIENTATION_EN_HORIZONTAL){

                    int left = getPaddingLeft();
                    int dragRange = mHorizontalDragRange == 0 ? getWidth() : mHorizontalDragRange;
                    if(mDirectionMove == DIRECTION_REVERSE) {
                        if (xvel < 0 || (xvel == 0 && releasedChild.getRight() < (float) dragRange / 2)) {
                            left -= dragRange;
                        }
                    }
                    else{
                        if (xvel > 0 || (xvel == 0 && releasedChild.getLeft() > (float) dragRange / 2)) {
                            left += dragRange;
                        }
                    }
                    mDragHelper.settleCapturedViewAt(left, releasedChild.getTop());

                    if(mViewReleaseListener != null)
                        mViewReleaseListener.onDragHorizontalRelease(releasedChild, xvel, yvel);

                    mTrackingTargetBound.set(
                            left,
                            releasedChild.getTop(),
                            left + releasedChild.getWidth(),
                            releasedChild.getBottom()
                    );
                }
                else if(mOrientation == DRAG_ORIENTATION_EN_VERTICAL){

                    int top = getPaddingTop();
                    int dragRange = mVerticalDragRange == 0 ? getHeight() : mVerticalDragRange;
                    if(mDirectionMove == DIRECTION_REVERSE) {
                        if (yvel < 0 || (yvel == 0 && releasedChild.getBottom() < (float) dragRange / 2)) {
                            top -= dragRange;
                        }
                    }
                    else{
                        if (yvel > 0 || (yvel == 0 && releasedChild.getTop() > (float) dragRange / 2)) {
                            top += dragRange;
                        }
                    }
                    mDragHelper.settleCapturedViewAt(releasedChild.getLeft(), top);

                    if(mViewReleaseListener != null)
                        mViewReleaseListener.onDragVerticalRelease(releasedChild, xvel, yvel);

                    mTrackingTargetBound.set(
                            releasedChild.getLeft(),
                            top,
                            releasedChild.getRight(),
                            top + getHeight()
                    );
                }
                else{
                    int top = getPaddingTop();
                    int left = getPaddingLeft();
                    int v_dragRange = mVerticalDragRange == 0 ? getHeight() : mVerticalDragRange;
                    int h_dragRange = mHorizontalDragRange == 0 ? getWidth() : mHorizontalDragRange;
                    if (yvel > 0 || (yvel == 0 && releasedChild.getTop() > (float)v_dragRange / 2)) {
                        top += v_dragRange;
                    }
                    if (xvel > 0 || (xvel == 0 && releasedChild.getLeft() > (float)h_dragRange / 2)) {
                        left += h_dragRange;
                    }
                    mDragHelper.settleCapturedViewAt(left, top);

                    if(mViewReleaseListener != null)
                        mViewReleaseListener.onDragAllDirectionRelease(releasedChild, xvel, yvel);
                }
                postInvalidate();
            }

            @Override
            public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
                super.onViewPositionChanged(changedView, left, top, dx, dy);
                postInvalidate();
                int l, t, r, b;
                l = left;
                t = top;
                r = l + mTargetChildView.getWidth();
                b = t + mTargetChildView.getHeight();
                mTrackingTargetPosition.set(l, t, r, b);
            }

        };
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        /**
         * Should intercept when
         * - Touch Edge
         * - Touch on Target
         * - Confirm from user
         */
        if(!isTargetTouch(ev)){
            mShouldIntercept = mDragHelper.shouldInterceptTouchEvent(ev) || isEdgeTouch(ev);
            return mShouldIntercept;
        }
        else if(isTargetTouch(ev) && isTargetViewFullVisible()){   //If Touch on target child
            if(mOnConfirmInterceptTouchListener != null){
                boolean confirm = mOnConfirmInterceptTouchListener.confirmInterceptTouch(ev);
                if(confirm){
                    mShouldIntercept = mDragHelper.shouldInterceptTouchEvent(ev) && confirm;
                    return mShouldIntercept;
                }
                else{
                    mShouldIntercept = false;
                    return super.onInterceptTouchEvent(ev);
                }
            }
        }

        mShouldIntercept = mDragHelper.shouldInterceptTouchEvent(ev);
        return mShouldIntercept;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //If not intercept then working normal behavior
        if(!mShouldIntercept) {
            return super.onTouchEvent(event);
        }

        boolean confirm = true;
        if(mOnConfirmTouchListener != null){
            confirm = mOnConfirmTouchListener.confirmTouch(event);
        }

        if(!confirm) {
            mDragHelper.processTouchEvent(event);
            return false;
        }
        else {
            mDragHelper.processTouchEvent(event);
            return confirm;
        }
    }

    @Override
    public void computeScroll() {
        if(mDragHelper.continueSettling(true)){
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    private boolean isTargetTouch(MotionEvent ev){
        Rect hitRect = new Rect();
        mTargetChildView.getHitRect(hitRect);
//        hitRect.left += getLeft();
//        hitRect.top += getTop();
//        hitRect.right += getLeft();
//        hitRect.bottom += getTop();
        int tx = Math.round(ev.getX());
        int ty = Math.round(ev.getY());
        return hitRect.contains(tx, ty);
    }

    private boolean isEdgeTouch(MotionEvent ev){
        Rect edgeBound = new Rect();
        getEdgeRect(edgeBound, mEdgeTracking);
        int tx = Math.round(ev.getX());
        int ty = Math.round(ev.getY());
        return edgeBound.contains(tx, ty);
    }

    private void getEdgeRect(Rect outRect, int edgeId){
        int edgeSize = mDragHelper.getEdgeSize();
        if(edgeId == ViewDragHelper.EDGE_LEFT){
            outRect.set(0, 0, edgeSize, getHeight());
        }
        else if(edgeId == ViewDragHelper.EDGE_TOP){
            outRect.set(0, 0, getWidth(), edgeSize);
        }
        else if(edgeId == ViewDragHelper.EDGE_RIGHT){
            outRect.set(getWidth() - edgeSize, 0, getWidth(), getHeight());
        }
        else if(edgeId == ViewDragHelper.EDGE_BOTTOM){
            outRect.set(0, getHeight() - edgeSize, getWidth(), getHeight());
        }
    }

    public interface OnViewRelease{
        void onDragHorizontalRelease(View view, float xVelo, float yVelo);
        void onDragVerticalRelease(View view, float xVelo, float yVelo);
        void onDragAllDirectionRelease(View view, float xVelo, float yVelo);
    }

    public interface OnConfirmInterceptTouch{
        boolean confirmInterceptTouch(MotionEvent ev);
    }

    public interface OnConfirmTouch{
        boolean confirmTouch(MotionEvent ev);
    }
}
