package com.rerr.simplelib.widget.layout;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Xfermode;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.rerr.simplelib.widget.util.QuickPath;

import java.lang.ref.SoftReference;

/**
 * Created by Rerr on 6/26/2015.
 * Description
 * -> describe here
 */
public class HexShapeLayout extends RelativeLayout {

    /**
     * Drawing items
     */
    private final Path mShapePath = QuickPath.HEXSMOOTH;
    private final RectF mRect = new RectF();
    private final RectF mRealShapeRect = new RectF();
    private final Paint mPaint = new Paint();
    private final Matrix mScaleShapeMatrix = new Matrix();
    private final Xfermode mXferMode = new PorterDuffXfermode(PorterDuff.Mode.DST_IN);

    /**
     * Temporary Canvas
     */
    private final Canvas mTempCv = new Canvas();
    private SoftReference<BitmapShader> mBitmapShader;

    /**
     * Maximum support child
     */
    private final int mLimitChild = 1;
    private SoftReference<Bitmap> mBmp;

    public HexShapeLayout(Context context) {
        this(context, null);
    }

    public HexShapeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HexShapeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
//        if(!isInEditMode())
            setWillNotDraw(true);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HexShapeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
//        if(!isInEditMode())
            setWillNotDraw(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        int widthSize, heightSize;
//
//        //Get the width based on the measure specs
//        widthSize = getDefaultSize(0, widthMeasureSpec);
//
//        //Get the height based on measure specs
//        heightSize = getDefaultSize(0, heightMeasureSpec);
//
//        int majorDimension = Math.min(widthSize, heightSize);
//        int childMeasureSpec = MeasureSpec.makeMeasureSpec(majorDimension, MeasureSpec.EXACTLY);
//        //Measure all child views
//        measureChildren(childMeasureSpec, childMeasureSpec);
//
//        //MUST call this to save our own dimensions
//        setMeasuredDimension(majorDimension, majorDimension);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
//        if(getChildCount() > mLimitChild){
//            throw new UnsupportedOperationException("Max child is " + mLimitChild);
//        }
//
//        for(int i=0; i<getChildCount(); i++){
//            View child = getChildAt(i);
//            child.layout(0, 0, child.getMeasuredWidth(), child.getMeasuredHeight());
//        }
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {

        mRect.left = getPaddingLeft();
        mRect.top = getPaddingTop();
        mRect.right = getMeasuredWidth() - getPaddingRight() - getPaddingLeft();
        mRect.bottom = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();

        int bw = (int) mRect.width();
        int bh = (int) mRect.height();

        mBmp = new SoftReference<>(Bitmap.createBitmap(bw, bh, Bitmap.Config.ARGB_8888));
        mTempCv.setBitmap(mBmp.get());

        super.dispatchDraw(mTempCv);

        mPaint.setAntiAlias(true);
        mBitmapShader = new SoftReference<>(new BitmapShader(mBmp.get(), Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

        Matrix mScaleShapeMatrix = new Matrix();
        RectF realBound = new RectF();
        mShapePath.computeBounds(realBound, true);
        mScaleShapeMatrix.setRectToRect(realBound, mRect, Matrix.ScaleToFit.CENTER);
        mShapePath.transform(mScaleShapeMatrix);

        mPaint.setShader(mBitmapShader.get());
        canvas.drawPath(mShapePath, mPaint);

        //Clear canvas
//        mTempCv.drawColor(0, PorterDuff.Mode.SRC_IN);

        //Clear Bitmap
        mBmp.get().recycle();
        mBmp.clear();
        mBitmapShader.clear();
        mBitmapShader = null;
        mBmp = null;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
    }
}
