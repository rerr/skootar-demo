package com.rerr.simplelib.widget.drawable;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Build;

import com.rerr.simplelib.widget.util.QuickShader;

/**
 * Created by Rerr on 6/27/2015.
 * Description
 * -> describe here
 */
public class ShapeImageDrawableFactory{

    private static final String TAG = ShapeImageDrawableFactory.class.getSimpleName();

    private static final ShapeImageDrawable.EffectShader EFFECT_SHADER = new ShapeImageDrawable.EffectShader() {
        @Override
        public Shader createShader(RectF bound) {
            return QuickShader.vignetteShader(bound);
        }

        @Override
        public PorterDuff.Mode createPorterDuffMode() {
            return PorterDuff.Mode.SRC_OVER;
        }
    };

    public static Drawable createOvalDrawable(Drawable original, int margin){
        return new ShapeImageDrawable(original, margin) {
            @Override
            protected void drawShape(Canvas canvas, Paint paint, RectF bound) {
                canvas.drawOval(bound, paint);
            }
        };
    }

    public static Drawable createOvalDrawable(Bitmap bitmap, int margin){
        return new ShapeImageDrawable(bitmap, margin) {
            @Override
            protected void drawShape(Canvas canvas, Paint paint, RectF bound) {
                canvas.drawOval(bound, paint);
            }
        };
    }

    public static Drawable createRoundRectDrawable(Drawable original, int margin,
                                                   final float rx, final float ry){
        return new ShapeImageDrawable(original, margin) {
            @Override
            protected void drawShape(Canvas canvas, Paint paint, RectF bound) {
                canvas.drawRoundRect(bound, rx, ry, paint);
            }
        };
    }

    public static Drawable createRoundRectDrawable(Bitmap bitmap, int margin,
                                                   final float rx, final float ry){
        return new ShapeImageDrawable(bitmap, margin) {
            @Override
            protected void drawShape(Canvas canvas, Paint paint, RectF bound) {
                canvas.drawRoundRect(bound, rx, ry, paint);
            }
        };
    }

    public static Drawable createCustomShapeDrawable(Drawable original, final int margin,
                                                     final Path customShape){
        ShapeImageDrawable newDrawable = new ShapeImageDrawable(original, margin) {
            @Override
            protected void drawShape(Canvas canvas, Paint paint, RectF bound) {
                Matrix m = new Matrix();
                RectF realBound = new RectF();
                customShape.computeBounds(realBound, true);
                m.setRectToRect(realBound, bound, Matrix.ScaleToFit.CENTER);
                customShape.transform(m);
                canvas.drawPath(customShape, paint);
            }
        };
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            newDrawable.setDisableShader(true);

        newDrawable.setEffectShader(EFFECT_SHADER);
        return newDrawable;
    }

    public static Drawable createCustomShapeDrawable(Bitmap bitmap, final int margin,
                                                     final Path customShape){
        ShapeImageDrawable newDrawable = new ShapeImageDrawable(bitmap, margin) {
            @Override
            protected void drawShape(Canvas canvas, Paint paint, RectF bound) {
                Matrix m = new Matrix();
                RectF realBound = new RectF();
                customShape.computeBounds(realBound, true);
                m.setRectToRect(realBound, bound, Matrix.ScaleToFit.CENTER);
                customShape.transform(m);
                canvas.drawPath(customShape, paint);
            }
        };
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            newDrawable.setDisableShader(true);

        newDrawable.setEffectShader(EFFECT_SHADER);
        return newDrawable;
    }

}
