package com.rerr.simplelib.widget.drawable;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ComposeShader;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

/**
 * Created by Rerr on 6/27/2015.
 * Description
 * -> describe here
 */
abstract public class ShapeImageDrawable extends Drawable{
    private static final String TAG = ShapeImageDrawable.class.getSimpleName();

    //Target Result Bound
    private final RectF mRect = new RectF();

    //Bitmap Bound
    private final RectF mBitmapRec = new RectF();

    //Image Shader
    private BitmapShader mBitmapShader;

    //Shader Painter
    private final Paint mPaint = new Paint();

    //Margin of drawable
    private int mMargin;

    //Check is BitmapDrawable
    private boolean mHaveBitmap = false;

    //Disable apply shader to image
    private boolean mDisableShader = false;

    public boolean isDisableShader() {
        return mDisableShader;
    }

    public void setDisableShader(boolean isDisable) {
        this.mDisableShader = isDisable;
    }

    //Custom Shader
    private EffectShader mEffectShader = null;

    //Set Custom Shader Interface
    public void setEffectShader(EffectShader effectShader) {
        this.mEffectShader = effectShader;
    }


    private void init(Bitmap bitmap, int margin){
        mBitmapShader = new BitmapShader(bitmap,
                Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

        mPaint.setAntiAlias(true);
        mPaint.setShader(mBitmapShader);

        mMargin = margin;
        mBitmapRec.set(0, 0, bitmap.getWidth(), bitmap.getHeight());

    }

    public ShapeImageDrawable(Drawable drawable, int margin) {
        mMargin = margin;

        if(drawable instanceof ColorDrawable){
            mHaveBitmap = false;
            mPaint.setColor(((ColorDrawable) drawable).getColor());
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setAntiAlias(true);
        }
        if(drawable instanceof BitmapDrawable){
            mHaveBitmap = true;
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            init(bitmapDrawable.getBitmap(), 0);
        }
    }

    public ShapeImageDrawable(Drawable drawable){
        this(drawable, 0);
    }

    public ShapeImageDrawable(Bitmap bitmap, int margin) {
        mHaveBitmap = true;
        mMargin = margin;
        init(bitmap, margin);
    }

    private RectF cropCenterRect(RectF src, RectF dst){
        float sw = src.right - src.left;
        float sh = src.bottom - src.top;
        float dw = dst.right - dst.left;
        float dh = dst.bottom - dst.top;
        float rw = dw / sw;
        float rh = dh / sh;
        if(sh * rw >= dh)
            return new RectF(0, 0, dw, sh * rw);
        else{
            return new RectF(0, 0, sw * rh, dh);
        }
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        mRect.set(mMargin, mMargin, bounds.width() - mMargin, bounds.height() - mMargin);

        if(!mHaveBitmap)
            return;

        Matrix m = new Matrix();
        mBitmapShader.getLocalMatrix(m);
        RectF cropRect = cropCenterRect(mBitmapRec, mRect);
        m.setRectToRect(mBitmapRec, cropRect, Matrix.ScaleToFit.CENTER);
        mBitmapShader.setLocalMatrix(m);

        if (mEffectShader != null && !mDisableShader) {
            Shader customShade = mEffectShader.createShader(mRect);
            PorterDuff.Mode duffMode = mEffectShader.createPorterDuffMode();
            mPaint.setShader(
                    new ComposeShader(mBitmapShader, customShade,
                            duffMode != null ? duffMode : PorterDuff.Mode.SRC_OVER));
        }
        else if(!mDisableShader){
            RadialGradient vignette = new RadialGradient(
                    mRect.centerX(), mRect.centerY(), mRect.centerX() * 1.7f,
                    new int[] { 0, 0, 0x4f000000 }, new float[] { 0.0f, 0.7f, 1.0f },
                    Shader.TileMode.CLAMP);

            Matrix oval = new Matrix();
            oval.setScale(1.0f, 0.7f);
            vignette.setLocalMatrix(oval);

            mPaint.reset();
            mPaint.setShader(
                    new ComposeShader(mBitmapShader, vignette, PorterDuff.Mode.SRC_OVER));
        }
        else{
            mPaint.setFilterBitmap(true);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        drawShape(canvas, mPaint, mRect);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        mPaint.setColorFilter(cf);
    }

    /**
     * Draw outline shape of drawable
     * @param canvas is provided canvas to draw primitive
     * @param paint is provided Paint to draw shader
     */
    protected abstract void drawShape(Canvas canvas, Paint paint, RectF bound);

    public interface EffectShader{
        Shader createShader(RectF bound);
        PorterDuff.Mode createPorterDuffMode();
    }

}
