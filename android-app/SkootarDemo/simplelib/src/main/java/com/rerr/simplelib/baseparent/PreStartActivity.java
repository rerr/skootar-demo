package com.rerr.simplelib.baseparent;

import android.support.v7.widget.Toolbar;
import android.view.View;

/**
 * Created by Rerr on 7/1/2015.
 * Description
 * -> describe here
 */
public interface PreStartActivity {
    void initToolbar();
    void init();
}
