package com.rerr.simplelib.widget.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;

import com.rerr.simplelib.widget.util.QuickPath;
import com.rerr.simplelib.widget.drawable.ShapeImageDrawableFactory;

/**
 * Created by Rerr on 6/28/2015.
 * Description
 * -> describe here
 */
public class HexImageView extends ShapeImageView {

    public HexImageView(Context context) {
        super(context);
    }

    public HexImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HexImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HexImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected Drawable createShapeDrawable(Drawable drawable, int margin) {
        return ShapeImageDrawableFactory.createCustomShapeDrawable(drawable, margin, QuickPath.HEXSMOOTH);
    }

    @Override
    protected Drawable createShapeDrawable(Bitmap bitmap, int margin) {
        return ShapeImageDrawableFactory.createCustomShapeDrawable(bitmap, margin, QuickPath.HEXSMOOTH);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
