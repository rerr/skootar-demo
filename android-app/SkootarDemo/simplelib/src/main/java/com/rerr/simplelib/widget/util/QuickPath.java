package com.rerr.simplelib.widget.util;

import android.graphics.Path;

/**
 * Created by Rerr on 6/28/2015.
 * Description
 * -> describe here
 */
public class QuickPath {
    /*
    <path d="
    M 0,0
    C 4.78,2.76 12.54,2.76 17.321,0
    L 89.2,-41.5
    c 4.781,-2.76 8.66,-9.48 8.66,-15
    l 0,-83
    c 0,-5.52 -3.879,-12.24 -8.66,-15
    L 17.321,-196
    C 12.54,-198.76 4.78,-198.76 0,-196
    l -71.88,41.5
    c -4.78,2.76 -8.66,9.48 -8.66,15
    l 0,83
    c 0,5.52 3.88,12.24 8.66,15
    L 0,0
    Z"/>
     */
    public static final Path HEXSMOOTH = new Path();
    static {
        HEXSMOOTH.moveTo(0, 0);
        HEXSMOOTH.cubicTo(4.78f, 2.76f, 12.54f, 2.76f, 17.321f, 0);
        HEXSMOOTH.lineTo(89.2f, -41.5f);
        HEXSMOOTH.rCubicTo(4.781f, -2.76f, 8.66f, -9.48f, 8.66f, -15);
        HEXSMOOTH.rLineTo(0, -83.0f);
        HEXSMOOTH.rCubicTo(0, -5.52f, -3.879f, -12.24f, -8.66f, -15);
        HEXSMOOTH.lineTo(17.321f, -196f);
        HEXSMOOTH.cubicTo(12.54f, -198.76f, 4.78f, -198.76f, 0, -196f);
        HEXSMOOTH.rLineTo(-71.88f, 41.5f);
        HEXSMOOTH.rCubicTo(-4.78f, 2.76f, -8.66f, 9.48f, -8.66f, 15);
        HEXSMOOTH.rLineTo(0, 83);
        HEXSMOOTH.rCubicTo(0, 5.52f, 3.88f, 12.24f, 8.66f, 15);
        HEXSMOOTH.lineTo(0, 0);
        HEXSMOOTH.close();
    }

    /*<path d="
    m 0,0
    c 46.96,0 81.85,-19.37 94.04,-27.2 2.649,-1.71 5.45,-5.67 6.271,-8.71 3.08,-11.44 9.689,-38.88 9.689,-64.09 0,-25.209 -6.609,-52.651 -9.689,-64.09 -0.821,-3.039 -3.641,-6.959 -6.321,-8.631
    C 81.529,-180.5 45.73,-200 0,-200
    c -45.73,0 -81.53,19.5 -93.99,27.279 -2.68,1.672 -5.5,5.592 -6.32,8.631 -3.08,11.439 -9.69,38.881 -9.69,64.09 0,25.21 6.61,52.65 9.69,64.09 0.82,3.04 3.62,7 6.27,8.71
    C -81.85,-19.37 -46.96,0 0,0"/>*/

    public static final Path TRIANGLE = new Path();
    private static final float RADIUS = 10;
    static {
        TRIANGLE.moveTo(-RADIUS, RADIUS);
        TRIANGLE.lineTo(RADIUS, RADIUS);
        TRIANGLE.lineTo(0, -RADIUS);
        TRIANGLE.lineTo(-RADIUS, RADIUS);
    }
}
