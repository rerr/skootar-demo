package com.rerr.simplelib.widget.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.rerr.simplelib.widget.drawable.ShapeImageDrawable;

import java.lang.ref.SoftReference;

/**
 * Created by Rerr on 6/27/2015.
 * Description
 * -> describe here
 */
public class CircleImageView extends ImageView{
    private static final String TAG = CircleImageView.class.getSimpleName();
    /**
     * Keep original drawable
     */
    private Drawable mOriginDrawable;

    public CircleImageView(Context context) {
        super(context);
    }

    public CircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircleImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CircleImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        mOriginDrawable = drawable;
        if(drawable instanceof TransitionDrawable) {
            TransitionDrawable td = (TransitionDrawable) drawable;

            //Setup ID to layers
            for(int i=0; i<td.getNumberOfLayers(); i++){
                td.setId(i, i);
            }

            //Replace circular drawable to all layer
            for(int i=0; i<td.getNumberOfLayers(); i++){
                SoftReference<Drawable> softDrawable =
                        new SoftReference<Drawable>(new CircularDrawable(td.findDrawableByLayerId(i), 5));
                CircularDrawable c = (CircularDrawable) softDrawable.get();
                td.setDrawableByLayerId(i, c);
            }
            super.setImageDrawable(td);
        }
        else {
            SoftReference<Drawable> softDrawable = new SoftReference<Drawable>(new CircularDrawable(drawable, 5));
            super.setImageDrawable(softDrawable.get());
        }
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        SoftReference<Drawable> softDrawable = new SoftReference<Drawable>(new CircularDrawable(bm, 5));
        super.setImageDrawable(softDrawable.get());
    }

    @Override
    public Drawable getDrawable() {
        return mOriginDrawable;
    }

    @Override
    public void setBackgroundDrawable(Drawable background) {
        SoftReference<Drawable> softDrawable = new SoftReference<Drawable>(new CircularDrawable(background, 5));
        super.setBackgroundDrawable(softDrawable.get());
    }

    @Override
    public void setBackground(Drawable background) {
        SoftReference<Drawable> softDrawable = new SoftReference<Drawable>(new CircularDrawable(background, 5));
        super.setBackground(softDrawable.get());
    }

    @Override
    protected boolean verifyDrawable(Drawable dr) {
        return super.verifyDrawable(dr) || dr instanceof TransitionDrawable;
    }

    class CircularDrawable extends ShapeImageDrawable {

        public CircularDrawable(Drawable drawable) {
            super(drawable);
        }

        public CircularDrawable(Drawable drawable, int margin) {
            super(drawable, margin);
        }

        public CircularDrawable(Bitmap bitmap, int margin) {
            super(bitmap, margin);
        }

        /**
         * Draw outline shape of drawable
         *
         * @param canvas is provided canvas to draw primitive
         * @param paint  is provided Paint to draw shader
         */
        @Override
        protected void drawShape(Canvas canvas, Paint paint, RectF bound) {
//        canvas.drawRoundRect(bound, 20, 20, paint);
            canvas.drawOval(bound, paint);
        }
    }

}
