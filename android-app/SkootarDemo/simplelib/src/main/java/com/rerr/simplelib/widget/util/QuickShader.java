package com.rerr.simplelib.widget.util;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;

/**
 * Created by Rerr on 6/28/2015.
 * Description
 * -> describe here
 */
public class QuickShader {
    public static Shader vignetteShader(RectF bound){
        RadialGradient vignette = new RadialGradient(
                bound.centerX(), bound.centerY(), bound.centerX() * 1.7f,
                new int[] { 0, 0, 0x4f000000 }, new float[] { 0.0f, 0.7f, 1.0f },
                Shader.TileMode.CLAMP);

        Matrix oval = new Matrix();
        oval.setScale(1.0f, 0.7f);
        vignette.setLocalMatrix(oval);
        return vignette;
    }

}
