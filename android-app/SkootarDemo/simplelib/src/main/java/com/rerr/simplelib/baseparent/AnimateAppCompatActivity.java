package com.rerr.simplelib.baseparent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.rerr.simplelib.R;

/**
 * Created by Rerr on 6/17/2015.
 * Description
 * -> describe here
 */
public abstract class AnimateAppCompatActivity extends AppCompatActivity
implements PreStartActivity {

    private boolean mIgnorePausedTransition = false;

    protected void setIgnorePausedTransition(boolean isIgnore){
        mIgnorePausedTransition = isIgnore;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(!mIgnorePausedTransition) {
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }
}
