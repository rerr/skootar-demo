package com.rerr.skootardemo.base;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.SphericalUtil;
import com.rerr.skootardemo.helper.MapAwesome;


/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BeginGoogleMapFragment extends BeginFragment {

    public static final String TAG = BeginGoogleMapFragment.class.getSimpleName();

    /**
     * Layout View ID
     */
    private int mLayoutId;

    /**
     * Map
     */
    protected MapView mMapObj;
    protected GoogleMap mMap;
    protected GoogleApiClient mGoogleClient;

    public BeginGoogleMapFragment() {
        // Required empty public constructor
    }

    protected abstract int getLayoutViewId();
    protected abstract MapView getMapViewObject(View contentView);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Init Save state
        initSaveViewState(savedInstanceState);

        // Inflate the layout for this fragment
        mLayoutId = getLayoutViewId();
        View v = inflater.inflate(mLayoutId, container, false);
        initToolbar(v);
        init(v);

        // Create map
        mMapObj = getMapViewObject(v);
        mMapObj.onCreate(savedInstanceState);

        setUpMapIfNeed();

        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapObj.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapObj.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapObj.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapObj.onLowMemory();
    }

    /**
     * Map setup and functions
     */
    protected void setUpMapIfNeed(){
        if(mMap == null){
            mMapObj.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                    mMap.setOnCameraChangeListener(onMapCamChange());
                    mMap.setOnMapClickListener(onMapClick());
                    mGoogleClient = MapAwesome.setupClientIfNeed(mGoogleClient,
                            getActivity().getApplicationContext(), onGoogleApiConnect());

                    setUpMapUi();
                    onMapLoaded(mMap);
                }
            });
        }
    }

    protected void onMapLoaded(GoogleMap map){

    }

    protected void setUpMapUi() {
        MapAwesome.setupUi(mMap);
    }

    protected GoogleApiClient.ConnectionCallbacks onGoogleApiConnect() {
        return new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
            }

            @Override
            public void onConnectionSuspended(int i) {
            }
        };
    }

    protected GoogleMap.OnCameraChangeListener onMapCamChange() {
        return new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

            }
        };
    }

    protected GoogleMap.OnMapClickListener onMapClick() {
        return new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

            }
        };
    }

}
