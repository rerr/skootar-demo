package com.rerr.skootardemo.widget.powerup;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.MotionEvent;
import android.view.ViewGroup;

import java.util.List;

public class SimpleViewPagerAdapter extends SmartFragmentStatePagerAdapter {

    List<Fragment> mPages;

    public SimpleViewPagerAdapter(FragmentManager fm, List<Fragment> pages) {
        super(fm);
        mPages = pages;
    }

    /**
     * Return the Fragment associated with a specified position.
     *
     * @param position
     */
    @Override
    public Fragment getItem(int position) {
        return mPages.get(position);
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return mPages.size();
    }
}
