package com.rerr.skootardemo.helper;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import org.apache.http.Header;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Rerr on 7/10/2015.
 * Description
 * -> describe here
 */
public class HttpAwesome {

    private static final String TAG = HttpAwesome.class.getSimpleName();

    public static String buildUrlString(String baseUrl, JSONObject params){
        try {

            String urlString = baseUrl;
            if(urlString.charAt(urlString.length() - 1) != '?'){
                urlString += "?";
            }

            if(params != null) {
                Iterator<String> rd = params.keys();
                while (rd.hasNext()) {
                    String key = rd.next();
                    String param = key + "=" + params.getString(key).replace(" ", "%20");
                    if (rd.hasNext()) {
                        param += "&";
                    }
                    urlString += param;
                }
            }

            URL url = new URL(urlString);
            return url.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getRest(String url, StringBuilder outResponse){
        int resCode = 0;
        try {
            URL urlCon = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlCon.openConnection();
            connection.setRequestMethod("GET");

            resCode = connection.getResponseCode();
            if(resCode == HttpURLConnection.HTTP_OK) {
                InputStream is = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                if (outResponse != null) {
                    StringBuilder response = outResponse;
                    String line;
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                }

                reader.close();
            }
            else{
                Log.v(TAG, connection.getResponseMessage());
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return resCode;
    }

    public static AsyncTask<Void, Void, Integer> getConnectTo(
            final String url, @Nullable final JSONObject params, final OnCompleteCallback callback){
        final StringBuilder response = new StringBuilder();
        return new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void[] objects) {
                String buildUrl = buildUrlString(url, params);
                return getRest(buildUrl, response);
            }

            @Override
            protected void onPostExecute(Integer resCode) {
                super.onPostExecute(resCode);
                if(callback != null){
                    callback.complete(response.toString());
                }
            }
        };
    }

    public interface OnCompleteCallback{
        void complete(String result);
    }
}
