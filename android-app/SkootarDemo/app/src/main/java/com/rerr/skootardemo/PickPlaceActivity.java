package com.rerr.skootardemo;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.rerr.simplelib.widget.layout.DragHelperLayout;
import com.rerr.skootardemo.base.BeginAppCompatActivity;
import com.rerr.skootardemo.controller.PickPlaceController;
import com.rerr.skootardemo.helper.MapAwesome;
import com.rerr.skootardemo.viewdata.PickPlaceViewData;
import com.rerr.skootardemo.widget.helper.TypeFaces;
import com.rerr.skootardemo.widget.powerup.CustomTypefaceSpan;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


public class PickPlaceActivity extends BeginAppCompatActivity
        implements View.OnClickListener{

    private static final String TAG = PickPlaceActivity.class.getSimpleName();
    private static final String VIEW_DATA = "view_data";

    /**
     * Controller and Data
     */
    private Controller mController = new PickPlaceController();
    private PickPlaceViewData mData = null;

    /**
     * Map
     */
    private RelativeLayout mMapContainer;
    private MapView mMapObj;
    private TextView mMapMsgBox;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleClient;
    private Location mLastLocation;
    private ProgressBar mMapProgressLoader;
    private LatLngBounds mInterestBound;
    private MarkerOptions mAwsomeMarker;

    /**
     * Search Toolbar
     */
    private EditText mSearchBox;
    private ImageView mBackNav;

    /**
     * Search Result list
     */
    private RecyclerView mSearchPlaceResultList;
    private SearchPlaceAdapter mSearchPlaceAdapter;
    private final List<SearchPlaceString> mSearchResultList = new ArrayList<>();
    private DragHelperLayout mSearchResultDragHelper;
    private ProgressBar mSearchProgressLoader;
    private View mBtnVisibleTrigger;
    private View mPoweredByGoogleLogo;
    private View mFooter;
    private AsyncTask<Void, Void, ArrayList<SearchPlaceString>> mSearchPlaceTask;

    /**
     * Flag to track set text in search box to avoid TextWatcher event
     */
    private boolean mIsAvoidTextChanged = false;

    /**
     * Selected Place Detail
     */
    private Place mLastSelectedPlace;
    private TextView mPlaceTitle;
    private TextView mPlaceAddress;
    private TextView mPlacePhone;
    private TextView mPlaceLatLng;

    /**
     * Button Done
     */
    private View mBtnDone;

    private void setController(Controller controller){
        mController = controller;
    }

    private void setData(PickPlaceViewData data){
        mData = data;
    }

    public static void start(Context fromContext, PickPlaceViewData data){
        Intent intent = new Intent(fromContext, PickPlaceActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(BeginAppCompatActivity.IGNORE_OVERRIDE_OPEN_TRANSITION, true);
        intent.putExtra(BeginAppCompatActivity.IGNORE_OVERRIDE_PAUSED_TRANSITION, true);
        intent.putExtra(VIEW_DATA, data == null ? null : data.get());
        fromContext.startActivity(intent);
    }

    public static void startForResult(Object fromCaller, PickPlaceViewData data, int requestCode){
        AppCompatActivity context = null;
        if(fromCaller instanceof Fragment) {
            context = (AppCompatActivity) ((Fragment) fromCaller).getActivity();
        }
        else{
            context = ((AppCompatActivity) fromCaller);
        }
        Intent intent = new Intent(context, PickPlaceActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(BeginAppCompatActivity.IGNORE_OVERRIDE_OPEN_TRANSITION, true);
        intent.putExtra(BeginAppCompatActivity.IGNORE_OVERRIDE_PAUSED_TRANSITION, true);
        if(data != null) {
            intent.putExtra(VIEW_DATA, data.get());
        }
        if(fromCaller instanceof Fragment){
            ((Fragment) fromCaller).startActivityForResult(intent, requestCode);
        }
        else{
            context.startActivityForResult(intent, requestCode);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBundle(VIEW_DATA, mData.get());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_place);
        initSaveViewState(savedInstanceState);
        initToolbar();
        init();
        displayPlaceByViewData(mData);

        // Create map
        if(isMapView(mMapObj))
            mMapObj.onCreate(savedInstanceState);

        setUpMapIfNeed();
    }

    private  boolean isMapView(Object testObject){
        if(testObject instanceof MapView)
            return true;
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapObj.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isMapView(mMapObj))
            mMapObj.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        freeMap();
        MapAwesome.freeMap(mMap);
        if(isMapView(mMapObj))
            mMapObj.onDestroy();
    }

    @Override
    public void onLowMemory(){
        super.onLowMemory();
        if(isMapView(mMapObj))
           mMapObj.onLowMemory();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_pick_place, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(mSearchResultDragHelper.isTargetViewVisible()){
            mSearchResultDragHelper.hideTargetView(true);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void initSaveViewState(Bundle savedInstanceState) {
        if(savedInstanceState != null){
            if(mData == null)
                mData = PickPlaceViewData.build();
            mData.set(savedInstanceState.getBundle(VIEW_DATA));
        }
        else if(getIntent().hasExtra(VIEW_DATA)) {
            if(mData == null)
                mData = PickPlaceViewData.build();
            mData.set(getIntent().getBundleExtra(VIEW_DATA));
        }
    }

    @Override
    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_toolbar).findViewById(R.id.awesome_toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);

        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setCustomView(R.layout.cab_search_toolbar);
        ab.setDisplayShowCustomEnabled(true);
        View v = ab.getCustomView();
        mSearchBox = (EditText) v.findViewById(R.id.input_search_box);
        TextWatcher tw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(mIsAvoidTextChanged) {
                    mIsAvoidTextChanged = false;
                    return;
                }

                if(mSearchPlaceTask != null){
                    mSearchPlaceTask.cancel(true);
                }
                mSearchPlaceTask = searchPlaceAsync(editable.toString());
                mSearchPlaceTask.execute();
            }
        };
        mSearchBox.addTextChangedListener(tw);
        mBackNav = (ImageView) v.findViewById(R.id.back_nav);
        mBackNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void init() {

        // UI display
        mPlaceTitle = (TextView) findViewById(R.id.pick_place_detail_title);
        mPlaceAddress = (TextView) findViewById(R.id.pick_place_detail_address);
        mPlaceLatLng = (TextView) findViewById(R.id.pick_place_detail_latlng);
        mPlacePhone = (TextView) findViewById(R.id.pick_place_detail_phone);

        // Done Button
        mBtnDone = findViewById(R.id.pick_place_btn_done);
        mBtnDone.setOnClickListener(this);

        // Map
        View embeddedMap = findViewById(R.id.app_embedded_map);
        mMapContainer = (RelativeLayout) embeddedMap;
        mMapObj = (MapView) embeddedMap.findViewById(R.id.map);
        mMapMsgBox = (TextView) embeddedMap.findViewById(R.id.map_msg_box);
        mMapMsgBox.setVisibility(View.GONE);

        // Search Result List
        final View listContainer = findViewById(R.id.pick_place_search_result_list);
        mFooter = listContainer.findViewById(R.id.footer);
        mSearchProgressLoader = (ProgressBar) listContainer.findViewById(R.id.search_progress);
        mSearchProgressLoader.setVisibility(View.INVISIBLE);
        mBtnVisibleTrigger = listContainer.findViewById(R.id.display_switch);
        mPoweredByGoogleLogo = listContainer.findViewById(R.id.logo_powered_by_google);
        mSearchPlaceResultList = (RecyclerView) listContainer.findViewById(R.id.rv_map_search_result_list);
        final LinearLayoutManager lm = new LinearLayoutManager(this);
        mSearchPlaceResultList.setLayoutManager(lm);

        mSearchPlaceAdapter = new SearchPlaceAdapter(mSearchResultList);
        mSearchPlaceAdapter.setOnSelectListener(onSelectItemListener());
        mSearchPlaceResultList.setAdapter(mSearchPlaceAdapter);

        //Drag Helper
        mSearchResultDragHelper = (DragHelperLayout) findViewById(R.id.pick_place_drag_helper);
        mSearchResultDragHelper.setDragOrientation(DragHelperLayout.DRAG_ORIENTATION_EN_VERTICAL, DragHelperLayout.DIRECTION_REVERSE);
        mSearchResultDragHelper.setEdgeTrackingEnable(ViewDragHelper.EDGE_TOP);
        mSearchResultDragHelper.setTargetChildView(listContainer);
        mSearchResultDragHelper.setVisibleOnStart(false);
        mSearchResultDragHelper.setOnConfirmInterceptTouchListener(new DragHelperLayout.OnConfirmInterceptTouch() {
            @Override
            public boolean confirmInterceptTouch(MotionEvent ev) {
                if (!mSearchResultDragHelper.isTargetViewFullVisible())
                    return true;

                Rect footRect = new Rect();
                mFooter.getHitRect(footRect);
                int tx = Math.round(ev.getX());
                int ty = Math.round(ev.getY());
                if(footRect.contains(tx, ty)){
                    return true;
                }

                return lm.findLastCompletelyVisibleItemPosition() == mSearchPlaceAdapter.getItemCount() - 1;
            }
        });
        mMapProgressLoader = (ProgressBar) findViewById(R.id.pick_place_pg_map_load);
        hideMapProgress();
    }

    private OnRecyclerSelectItem onSelectItemListener() {
        return new OnRecyclerSelectItem() {
            @Override
            public void onSelectItem(View view, int position) {
                SearchPlaceString selectedPlace = mSearchResultList.get(position);
                displayPlaceDetailByPlaceId(selectedPlace.searchPlaceResultId);
                mIsAvoidTextChanged = true;
                mSearchBox.setText(selectedPlace.searchPlaceResultName);
                mSearchResultDragHelper.hideTargetView(false);
                hideSoftKeyboard();
            }
        };
    }

    /**
     * Set Select place to To Display
     */
    private void buildViewDataByPlace(Place place){
        mLastSelectedPlace = place;
        mData = PickPlaceViewData.build()
                .placeName(place.getName().toString())
                .placeLatLng(place.getLatLng())
                .placeAddress(place.getAddress().toString())
                .placePhone(place.getPhoneNumber().toString());
    }

    private void displayPlaceDetailByPlaceId(String googlePlaceId){
        Places.GeoDataApi
                .getPlaceById(mGoogleClient, googlePlaceId)
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess()) {
                            mLastSelectedPlace = places.get(0);
                            Log.i(TAG, "Place found: " + mLastSelectedPlace.getName());

                            buildViewDataByPlace(mLastSelectedPlace);

                            mPlaceTitle.setText(mLastSelectedPlace.getName());
                            mPlaceAddress.setText(mLastSelectedPlace.getAddress());
                            mPlaceLatLng.setText(mLastSelectedPlace.getLatLng().toString());
                            mPlacePhone.setText(mLastSelectedPlace.getPhoneNumber().toString());

                            MapAwesome.animateMapTo(mMap, mLastSelectedPlace.getLatLng(), mAwsomeMarker);
                        }
                        places.release();
                    }
                });

    }

    /**
     * Search place functions
     */
    private AsyncTask<Void, Void, ArrayList<SearchPlaceString>> searchPlaceAsync(final String searchWord){
        if(mSearchProgressLoader.getVisibility() != View.VISIBLE) {
            mSearchProgressLoader.setAlpha(0);
            mSearchProgressLoader.setVisibility(View.VISIBLE);
            mSearchProgressLoader.animate().alpha(1.0f).setDuration(200).setInterpolator(new DecelerateInterpolator()).start();
        }
        if(!mSearchResultDragHelper.isTargetViewVisible()) {
            mSearchResultDragHelper.slideTargetViewTo(
                    0,
                    -mSearchResultDragHelper.getTargetChildView().getHeight() + mFooter.getHeight() + mSearchProgressLoader.getHeight(),
                    true
            );
        }
        return new AsyncTask<Void, Void, ArrayList<SearchPlaceString>>() {
            @Override
            protected ArrayList<SearchPlaceString> doInBackground(Void... voids) {
                if(searchWord.equals(""))
                    return null;
                ArrayList<SearchPlaceString> result = getAutocomplete(searchWord);
                return result;
            }

            @Override
            protected void onPostExecute(ArrayList<SearchPlaceString> result) {
                super.onPostExecute(result);
                mSearchResultList.clear();
                if(result != null){
                    mSearchResultList.addAll(result);
                }
                mSearchPlaceAdapter.notifyDataSetChanged();
                mSearchProgressLoader.setVisibility(View.INVISIBLE);
                if(mSearchPlaceAdapter.getItemCount() > 0 && !searchWord.equals("")) {
                    mSearchResultDragHelper.showTargetView(true);
                }
                else if(mSearchPlaceAdapter.getItemCount() == 0 && searchWord.equals("")){
                    mSearchResultDragHelper.hideTargetView(true);
                }
            }
        };
    }

    private ArrayList<SearchPlaceString> getAutocomplete(CharSequence constraint) {
        if (mGoogleClient.isConnected()) {
            Log.i(TAG, "Starting autocomplete query for: " + constraint);
            // Submit the query to the autocomplete API and retrieve a PendingResult that will
            // contain the results when the query completes.
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleClient, constraint.toString(),
                                    mInterestBound, null);

            // This method should have been called off the main UI thread. Block and wait for at most 60s
            // for a result from the API.
            AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);

            final com.google.android.gms.common.api.Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                Log.e(TAG, "Error getting autocomplete prediction API call: " + status.toString());
                autocompletePredictions.release();
                return null;
            }

            Log.i(TAG, "Query completed. Received " + autocompletePredictions.getCount()
                    + " predictions.");

            // Copy the results into our own data structure, because we can't hold onto the buffer.
            // AutocompletePrediction objects encapsulate the API response (place ID and description).
            ArrayList<SearchPlaceString> searchList = new ArrayList<>(autocompletePredictions.getCount());
            Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
            while (iterator.hasNext()) {
                AutocompletePrediction prediction = iterator.next();
                // Get the details of this prediction and copy it into a new PlaceAutocomplete object.
                SearchPlaceString s = new SearchPlaceString();
                s.searchWord = constraint.toString();
                s.searchPlaceResultName = prediction.getDescription();
                s.searchPlaceResultId = prediction.getPlaceId();
                searchList.add(s);
            }

            // Release the buffer now that all data has been copied.
            autocompletePredictions.release();
            return searchList;
        }
        Log.e(TAG, "Google API client is not connected for autocomplete query.");
        return null;
    }

    private void displayCurrentPlace(){
        PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi
                .getCurrentPlace(mGoogleClient, null);
        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(PlaceLikelihoodBuffer likelyPlaces) {
                double minDistance = -1;
                LatLng lastLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                    Log.i(TAG, String.format("Place '%s' has likelihood: %g",
                            placeLikelihood.getPlace().getName(),
                            placeLikelihood.getLikelihood()));

                    Place p = placeLikelihood.getPlace();
                    if(minDistance == -1){
                        minDistance = SphericalUtil.computeDistanceBetween(lastLocation, p.getLatLng());
                        mLastSelectedPlace = p;
                    }
                    else{
                        double newDist = SphericalUtil.computeDistanceBetween(lastLocation, p.getLatLng());
                        if(newDist < minDistance){
                            mLastSelectedPlace = p;
                            minDistance = newDist;
                        }
                    }
                }

                mData = PickPlaceViewData.build()
                            .placeName(mLastSelectedPlace.getName().toString())
                            .placeAddress(mLastSelectedPlace.getAddress().toString())
                            .placeLatLng(mLastSelectedPlace.getLatLng())
                            .placePhone(mLastSelectedPlace.getPhoneNumber().toString());
                    displayPlaceByViewData(mData);

                likelyPlaces.release();
            }
        });
    }

    /**
     * Map setup and functions
     */
    private void setUpMapIfNeed(){
        MapAwesome.hideMapView(mMapObj);
        showMapProgress();
        if(mMap == null){
            mMapObj.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                    MapAwesome.setupUi(mMap);
                    mMap.setOnCameraChangeListener(onMapCamChange());
                    mMap.setOnMapClickListener(onMapClick());
                    mGoogleClient = MapAwesome.setupClientIfNeed(mGoogleClient,
                            getApplicationContext(), onLocationConnect());
                    mAwsomeMarker = MapAwesome.buildMarkerOption(getResources(), R.drawable.ic_marker_default);
                }
            });
        }
    }

    private GoogleApiClient.ConnectionCallbacks onLocationConnect() {
        return new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                hideMapProgress();
                MapAwesome.fadeInMapView(mMapObj);
                moveMapToCurrentLocation().execute();
                if(mData == null){
                    displayCurrentPlace();
                }
            }

            @Override
            public void onConnectionSuspended(int i) {
                MapAwesome.fadeInMapView(mMapObj);
                hideMapProgress();
            }
        };
    }

    public GoogleMap.OnCameraChangeListener onMapCamChange() {
        return new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

            }
        };
    }

    public GoogleMap.OnMapClickListener onMapClick() {
        return new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

            }
        };
    }

    private AsyncTask<Void, Void, Void> moveMapToCurrentLocation(){
        return new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleClient);
                double interrestDistnace = 50 * 1000;
                LatLng origin = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                LatLng north_east = SphericalUtil.computeOffset(origin, interrestDistnace, 45);
                LatLng south_west = SphericalUtil.computeOffset(origin, interrestDistnace, 225);
                mInterestBound = new LatLngBounds(south_west, north_east);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if(mData != null && mData.getPlaceLatLng() != null){
                    MapAwesome.moveMapTo(mMap, mData.getPlaceLatLng());
                    return;
                }
                LatLng currentLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                MapAwesome.moveMapTo(mMap, currentLatLng, mAwsomeMarker);
            }
        };
    }

    private void showMapProgress(){
        mMapProgressLoader.setVisibility(View.VISIBLE);
    }

    private void hideMapProgress(){
        mMapProgressLoader.setVisibility(View.GONE);
    }

    private void displayPlaceByViewData(PickPlaceViewData viewData){
        if(viewData == null)
            return;

        mPlaceTitle.setText(viewData.getPlaceName());
        mPlaceAddress.setText(viewData.getPlaceAddress());
        mPlaceLatLng.setText(viewData.getPlaceLatLng().toString());
        mPlacePhone.setText(viewData.getPlacePhone());
    }

    @Override
    public void onClick(View view) {
        if(view == mBtnDone) {
            if (mController != null) {
                mController.onDoneClick(view, mData);
            }
            Intent resultData = new Intent();
            resultData.putExtra(PickPlaceViewData.class.getSimpleName(), mData.get());
            setResult(RESULT_OK, resultData);
            finish();
        }
    }

    /**
     * Controller
     */
    public interface Controller{
        void onDoneClick(View sender, PickPlaceViewData data);
    }

    /**
     * Search Place Adapter
     */
    private class SearchPlaceAdapter extends RecyclerView.Adapter<SearchPlaceViewHolder>{

        private List<SearchPlaceString> mDataSet;
        private OnRecyclerSelectItem mOnSelectListener;

        public void setOnSelectListener(OnRecyclerSelectItem listener){
            mOnSelectListener = listener;
        }

        public SearchPlaceAdapter(List<SearchPlaceString> dataSet){
            mDataSet = dataSet;
        }

        /**
         * Called when RecyclerView needs a new {@link SearchPlaceViewHolder} of the given type to represent
         * an item.
         * <p/>
         * This new ViewHolder should be constructed with a new View that can represent the items
         * of the given type. You can either create a new View manually or inflate it from an XML
         * layout file.
         * <p/>
         * The new ViewHolder will be used to display items of the adapter using
         * {@link #onBindViewHolder(SearchPlaceViewHolder, int)}. Since it will be re-used to display different
         * items in the data set, it is a good idea to cache references to sub views of the View to
         * avoid unnecessary {@link View#findViewById(int)} calls.
         *
         * @param parent   The ViewGroup into which the new View will be added after it is bound to
         *                 an adapter position.
         * @param viewType The view type of the new View.
         * @return A new ViewHolder that holds a View of the given view type.
         * @see #getItemViewType(int)
         * @see #onBindViewHolder(SearchPlaceViewHolder, int)
         */
        @Override
        public SearchPlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(getApplicationContext()).inflate(R.layout.list_item_simple_item, null);
            SearchPlaceViewHolder vh = new SearchPlaceViewHolder(v, mOnSelectListener);
            return vh;
        }

        /**
         * Called by RecyclerView to display the data at the specified position. This method
         * should update the contents of the {@link SearchPlaceViewHolder#itemView} to reflect the item at
         * the given position.
         * <p/>
         * Note that unlike {@link ListView}, RecyclerView will not call this
         * method again if the position of the item changes in the data set unless the item itself
         * is invalidated or the new position cannot be determined. For this reason, you should only
         * use the <code>position</code> parameter while acquiring the related data item inside this
         * method and should not keep a copy of it. If you need the position of an item later on
         * (e.g. in a click listener), use {@link SearchPlaceViewHolder#getPosition()} which will have the
         * updated position.
         *
         * @param holder   The ViewHolder which should be updated to represent the contents of the
         *                 item at the given position in the data set.
         * @param position The position of the item within the adapter's data set.
         */
        @Override
        public void onBindViewHolder(SearchPlaceViewHolder holder, int position) {
            holder.setData(mDataSet.get(position));
        }

        /**
         * Returns the total number of items in the data set hold by the adapter.
         *
         * @return The total number of items in this adapter.
         */
        @Override
        public int getItemCount() {
            return mDataSet.size();
        }
    }

    /**
     * Search Place ViewHolder
     */
    private class SearchPlaceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView mTitle;
        private OnRecyclerSelectItem mOnSelectListener;

        public SearchPlaceViewHolder(View itemView, OnRecyclerSelectItem onSelectListener) {
            super(itemView);
            mOnSelectListener = onSelectListener;
            itemView.setOnClickListener(this);
            mTitle = (TextView) itemView.findViewById(R.id.simple_title);
        }

        public void setData(SearchPlaceString data){
            //These used for check beginning index of search word
            String fullText = data.searchPlaceResultName.toLowerCase(Locale.getDefault());
            String critiria = data.searchWord.toLowerCase(Locale.getDefault());

            //Check start of search word
            int start = 0;
            if(fullText.contains(critiria)){
                start = fullText.indexOf(critiria, 0);
            }

            Spannable strSpan = new SpannableString(data.searchPlaceResultName);

            String fontString = getResources().getString(R.string.Quicksand_Bold);
            TypefaceSpan quicksand_bold = new CustomTypefaceSpan(
                    fontString,
                    TypeFaces.getTypeFace(getApplicationContext(), fontString)
            );
            ForegroundColorSpan white = new ForegroundColorSpan(Color.WHITE);
            ForegroundColorSpan gray= new ForegroundColorSpan(Color.LTGRAY);

            int end = Math.min(start + data.searchWord.length(), data.searchPlaceResultName.length());
            strSpan.setSpan(gray, 0, data.searchPlaceResultName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            strSpan.setSpan(quicksand_bold, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            strSpan.setSpan(white, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            mTitle.setText(strSpan);
        }

        @Override
        public void onClick(View view) {
            mOnSelectListener.onSelectItem(view, getPosition());
        }
    }

    /**
     * Search Place Result Item
     */
    private static class SearchPlaceString {
        public String searchWord;
        public String searchPlaceResultName;
        public String searchPlaceResultId;
        public static boolean isIntersectString(String fullStr, String partStr){
            String fullText = fullStr.toLowerCase(Locale.getDefault());
            String critiria = partStr.toLowerCase(Locale.getDefault());
            return fullText.contains(critiria);
        }
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

    /**
     * Interface for make list can selected on Click
     */
    private interface OnRecyclerSelectItem{
        void onSelectItem(View view, int position);
    }
}
