package com.rerr.skootardemo;

import android.content.Context;
import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.rerr.simplelib.baseparent.AnimateAppCompatActivity;
import com.rerr.simplelib.widget.powerup.SimplePageTransform;
import com.rerr.skootardemo.base.BeginAppCompatActivity;
import com.rerr.skootardemo.base.BeginFragment;
import com.rerr.skootardemo.controller.WriteCoverController;
import com.rerr.skootardemo.viewdata.MainViewData;
import com.rerr.skootardemo.viewdata.PickPlaceViewData;
import com.rerr.skootardemo.viewdata.SelectRouteViewData;
import com.rerr.skootardemo.widget.powerup.SimpleViewPagerAdapter;
import com.rerr.skootardemo.widget.powerup.SwipeBackPageTransform;
import com.rerr.skootardemo.widget.powerup.SwipeBackViewPager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BeginAppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String VIEW_DATA = "view_data";

    private static SwipeBackViewPager mPager;
    private SimpleViewPagerAdapter mPagerAdapter;
    private final List<Fragment> mPages = new ArrayList<>();
    private int mCurrentPagePosition = 0;

    private MainViewData mData = new MainViewData();

    public static void start(Context fromContext){
        Intent intent = new Intent(fromContext, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(BeginAppCompatActivity.IGNORE_OVERRIDE_OPEN_TRANSITION, true);
        intent.putExtra(BeginAppCompatActivity.IGNORE_OVERRIDE_PAUSED_TRANSITION, true);
        fromContext.startActivity(intent);
    }

    public static ViewPager getPager(){
        return mPager;
    }

    @Override
    protected void initSaveViewState(Bundle savedInstanceState) {
        if(savedInstanceState != null){
            if(mData == null)
                mData = new MainViewData();
            mData.set(savedInstanceState.getBundle(VIEW_DATA));
        }
    }

    @Override
    public void initToolbar() {
//        Toolbar toolbar = (Toolbar) findViewById(R.id.app_toolbar).findViewById(R.id.awesome_toolbar);
//        setSupportActionBar(toolbar);
    }

    @Override
    public void init() {
        mPages.add(WriteCoverFragment.createInstance());
        mPages.add(SelectRouteFragment.createInstance());
        mPages.add(ReviewFragment.createInstance());

        mPager = (SwipeBackViewPager) findViewById(R.id.main_viewpager);
        float defAbSzX = getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material);
        float defAbSzY = getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material);
        mPager.setExceptSwipeArea(new RectF(0, 0, defAbSzX, defAbSzY));
        mPager.setOffscreenPageLimit(3);
        mPagerAdapter = new SimpleViewPagerAdapter(getSupportFragmentManager(), mPages);
        mPager.setAdapter(mPagerAdapter);
        mPager.setPageTransformer(false, new SwipeBackPageTransform());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBundle(VIEW_DATA, mData.get());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setIgnoreOpenTransition(true);
        setIgnorePausedTransition(true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setIgnorePausedTransition(true);
        initSaveViewState(savedInstanceState);
        initToolbar();
        init();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity get AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public interface Controller{
        void onNextClick(View sender, MainViewData data);
    }

    @Override
    public void onBackPressed() {
        if(mPager.getCurrentItem() > 0){
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
        else{
            super.onBackPressed();
        }
    }
}
