package com.rerr.skootardemo.widget.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import com.rerr.skootardemo.R;
import com.rerr.skootardemo.widget.helper.TypeFaces;

/**
 * Created by Rerr on 6/15/2015.
 * Description
 * TextViewPlus is
 */
public class TextViewPlus extends TextView {

    private String mTypeface;
    private Context mContext;

    public TextViewPlus(Context context) {
        this(context, null);
    }

    public TextViewPlus(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextViewPlus(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initAttrs(attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TextViewPlus(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        initAttrs(attrs, defStyleAttr);
    }

    private void initAttrs(AttributeSet attrs, int defStyle){

        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.TextPlus, defStyle, 0);

        mTypeface = a.getString(R.styleable.TextPlus_typefaceName);
        if(mTypeface != null){
            Typeface tf = TypeFaces.getTypeFace(mContext, mTypeface);
            if(tf != null)
                this.setTypeface(tf);
        }
        a.recycle();
    }
}
