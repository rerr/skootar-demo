package com.rerr.skootardemo.controller;

import android.support.v4.view.ViewPager;
import android.view.View;

import com.rerr.skootardemo.MainActivity;
import com.rerr.skootardemo.ReviewFragment;
import com.rerr.skootardemo.SelectRouteFragment;
import com.rerr.skootardemo.WriteCoverFragment;
import com.rerr.skootardemo.viewdata.MainViewData;
import com.rerr.skootardemo.viewdata.WriteCoverViewData;
import com.rerr.skootardemo.widget.powerup.SmartFragmentStatePagerAdapter;

/**
 * Created by Rerr on 7/2/2015.
 * Description
 * -> describe here
 */
public class WriteCoverController implements WriteCoverFragment.Controller {
    @Override
    public void onNextClick(View sender, WriteCoverViewData data) {
        ViewPager pager = MainActivity.getPager();
        SmartFragmentStatePagerAdapter adapter = (SmartFragmentStatePagerAdapter) pager.getAdapter();
        SelectRouteFragment p = (SelectRouteFragment) adapter.getRegisteredFragment(pager.getCurrentItem() + 1);
        p.setCoverViewData(data);
        MainActivity.getPager().setCurrentItem(pager.getCurrentItem() + 1, true);
    }
}
