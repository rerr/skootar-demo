package com.rerr.skootardemo.base;

import android.os.Bundle;

/**
 * Created by Rerr on 7/3/2015.
 * Description
 * -> describe here
 */
public abstract class BeginViewData {

    protected Bundle mData = new Bundle();

    public Bundle get(){
        return mData;
    }

    public void set(Bundle bundle) {
        mData = bundle;
    }

}
