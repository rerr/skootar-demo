package com.rerr.skootardemo.base;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rerr.skootardemo.R;
import com.rerr.skootardemo.controller.ReviewController;
import com.rerr.skootardemo.viewdata.ReviewViewData;
import com.rerr.skootardemo.viewdata.SelectRouteViewData;


/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BeginFragment extends Fragment {

    public BeginFragment() {
        // Required empty public constructor
    }

    protected abstract void initSaveViewState(Bundle savedInstanceState);
    protected abstract void initToolbar(View v);
    protected abstract void init(View v);
}
