package com.rerr.skootardemo.widget.powerup;

import android.content.Context;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.rerr.skootardemo.R;

/**
 * Created by Rerr on 7/3/2015.
 * Description
 * -> describe here
 */
public class SwipeBackViewPager extends ViewPager {

    private static final String TAG = SwipeBackViewPager.class.getSimpleName();

    private static final int EDGE_NONE = -1;
    private static final int EDGE_LEFT = 0;
    private static final int EDGE_TOP = 1;
    private static final int EDGE_RIGHT = 2;
    private static final int EDGE_BOTTOM = 3;

    /**
     * Flag to track drag state
     */
    private static final int STATE_IDLE = 0;
    private static final int STATE_DRAG = 1;
    private static final int STATE_FORCE = 2;

    /**
     * Offset to discard checking swipe
     */
    private RectF mExceptSwipeArea = null;

    private int mDragState = STATE_IDLE;

    private int mTrackEdge = EDGE_LEFT;
    private int mSlopSz;
    private boolean mShouldIntercept = false;
    private int mCurrentPageTrack;
    private final RectF mOriginPageBound = new RectF();

    public int getSlopSz() {
        return mSlopSz;
    }

    public void setSlopSize(int touchSlopSize) {
        this.mSlopSz = touchSlopSize;
    }

    public int getTrackEdge() {
        return mTrackEdge;
    }

    public RectF getExceptSwipeArea() {
        return mExceptSwipeArea;
    }

    public void setExceptSwipeArea(RectF area) {
        this.mExceptSwipeArea = area;
    }

    /**
     * Set the edge to track since swipe
     * @param whatEdge is which one in EDGE_LEFT, EDGE_TOP, EDGE_RIGHT, EDGE_BOTTOM
     *                 if you want to disable tracking edge please set EDGE_NONE
     */
    public void setTrackEdge(int whatEdge) {
        this.mTrackEdge = whatEdge;
    }

    private final PointF mLastTouchPoint = new PointF();

    public SwipeBackViewPager(Context context) {
        this(context, null);
    }

    public SwipeBackViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void init(){
        mSlopSz = getResources().getDimensionPixelSize(R.dimen.edge_touch_slop_sz);
       /* addOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.d(TAG, String.format("position: %d posOffset: %f posOffsetPx: %d",
                        position, positionOffset, positionOffsetPixels));
                if(position == mCurrentPageTrack && positionOffsetPixels == 0){
                    mShouldIntercept = false;
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });*/
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        if(mDragState == STATE_FORCE){
            mDragState = STATE_IDLE;
            return super.onInterceptTouchEvent(ev);
        }

        if(mTrackEdge == EDGE_NONE || getCurrentItem() == 0)
            return false;

        mShouldIntercept = shouldInterceptSwipe(ev);
        return mShouldIntercept;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if(ev.getAction() == MotionEvent.ACTION_DOWN){
            mDragState = STATE_IDLE;
            return super.onTouchEvent(ev);
        }
        if(ev.getAction() == MotionEvent.ACTION_MOVE && mShouldIntercept) {

            mDragState = STATE_DRAG;
//            Log.d(TAG, "lastPointX: " + mLastTouchPoint.x + " newX: " + ev.getX() + " diff x = " + (ev.getX() - mLastTouchPoint.x));

            if (mTrackEdge == EDGE_LEFT  && isSwipeRight(new PointF(ev.getX(), ev.getY()))) {
                return super.onTouchEvent(ev);
            }
            else if (mTrackEdge == EDGE_TOP  && isSwipeDown(new PointF(ev.getX(), ev.getY()))) {
                return super.onTouchEvent(ev);
            }
            else if (mTrackEdge == EDGE_RIGHT  && isSwipeLeft(new PointF(ev.getX(), ev.getY()))) {
                return super.onTouchEvent(ev);
            }
            else if (mTrackEdge == EDGE_BOTTOM  && isSwipeUp(new PointF(ev.getX(), ev.getY()))) {
                return super.onTouchEvent(ev);
            }
            else{
                return false;
            }
        }
        if(ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_CANCEL){
            mLastTouchPoint.set(0, 0);
            if(mDragState == STATE_DRAG) {
                return super.onTouchEvent(ev);
            }
            else{
                MotionEvent downEvent = MotionEvent.obtain(ev);
                downEvent.setAction(MotionEvent.ACTION_DOWN);
                mShouldIntercept = false;
                mDragState = STATE_FORCE;
                return this.onInterceptTouchEvent(downEvent);
            }

        }
        return false;
    }

    private boolean shouldInterceptSwipe(MotionEvent ev){
        if(ev.getAction() == MotionEvent.ACTION_DOWN){
            RectF possibleArea = getEdgeSlopArea(mTrackEdge);
            if(mExceptSwipeArea != null && mExceptSwipeArea.contains(ev.getX(), ev.getY())){
                return false;
            }
            if(possibleArea != null && possibleArea.contains(ev.getX(), ev.getY())){
                mLastTouchPoint.x = ev.getX();
                mLastTouchPoint.y = ev.getY();
                return true;
            }
        }
        return false;
    }

    private boolean isReachDefaultBound(){
        View p = getChildAt(mCurrentPageTrack);
        if(mTrackEdge == EDGE_LEFT){
            return p.getLeft() == mOriginPageBound.left;
        }
        else if(mTrackEdge == EDGE_TOP){
            return p.getTop() == mOriginPageBound.top;
        }
        else if(mTrackEdge == EDGE_RIGHT){
            return p.getRight() == mOriginPageBound.right;
        }
        else if(mTrackEdge == EDGE_BOTTOM){
            return p.getBottom() == mOriginPageBound.bottom;
        }
        return false;
    }

    private boolean isSwipeRight(PointF currentTouch){
        return currentTouch.x - mLastTouchPoint.x > 0;
    }

    private boolean isSwipeLeft(PointF currentTouch){
        return currentTouch.x - mLastTouchPoint.x < 0;
    }

    private boolean isSwipeUp(PointF currentTouch){
        return currentTouch.y - mLastTouchPoint.y < 0;
    }

    private boolean isSwipeDown(PointF currentTouch){
        return currentTouch.y - mLastTouchPoint.y > 0;
    }

    private RectF getEdgeSlopArea(int whatEdge){
        if(whatEdge == EDGE_LEFT){
            RectF area = new RectF();
            area.left = getLeft();
            area.top = getTop();
            area.right = getLeft() + mSlopSz;
            area.bottom = getTop() + getHeight();
            return area;
        }
        if(whatEdge == EDGE_TOP){
            RectF area = new RectF();
            area.left = getLeft();
            area.top = getTop();
            area.right = getLeft() + getWidth();
            area.bottom = getTop() + mSlopSz;
            return area;
        }
        if(whatEdge == EDGE_RIGHT){
            RectF area = new RectF();
            area.left = getRight() - mSlopSz;
            area.top = getTop();
            area.right = getRight();
            area.bottom = getTop() + getHeight();
            return area;
        }
        if(whatEdge == EDGE_BOTTOM){
            RectF area = new RectF();
            area.left = getLeft();
            area.top = getBottom() - mSlopSz;
            area.right = getLeft() + getWidth();
            area.bottom = getBottom();
            return area;
        }
        return null;
    }
}
