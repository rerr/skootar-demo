package com.rerr.skootardemo.controller;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.google.android.gms.maps.SupportMapFragment;
import com.rerr.skootardemo.MainActivity;
import com.rerr.skootardemo.PickPlaceActivity;
import com.rerr.skootardemo.ReviewFragment;
import com.rerr.skootardemo.SelectRouteFragment;
import com.rerr.skootardemo.helper.DirectionAwesome;
import com.rerr.skootardemo.viewdata.ReviewViewData;
import com.rerr.skootardemo.viewdata.SelectRouteViewData;
import com.rerr.skootardemo.viewdata.WriteCoverViewData;
import com.rerr.skootardemo.widget.powerup.SmartFragmentStatePagerAdapter;

/**
 * Created by Rerr on 7/2/2015.
 * Description
 * -> describe here
 */
public class SelectRouteController implements SelectRouteFragment.Controller {

    @Override
    public void onOkClick(View sender,
                          SelectRouteViewData routeData, WriteCoverViewData coverData,
                          DirectionAwesome.DecodeResult directionRes) {
        ViewPager pager = MainActivity.getPager();
        pager.setCurrentItem(pager.getCurrentItem() + 1);

        SmartFragmentStatePagerAdapter adapter = (SmartFragmentStatePagerAdapter) pager.getAdapter();
        ReviewFragment p = (ReviewFragment) adapter.getRegisteredFragment(pager.getCurrentItem());
        ReviewViewData viewData = ReviewViewData.build()
                .senderName(coverData.getSenderName())
                .senderPhone(coverData.getSenderPhone())
                .receiverName(coverData.getReceiverName())
                .receiverPhone(coverData.getReceiverPhone())
                .fromAddr(routeData.getFromAddr())
                .fromLatLng(routeData.getFromLatLng())
                .toAddr(routeData.getToAddr())
                .toLatLng(routeData.getToLatLng())
                .distanceMeters(routeData.getDistanceMeters())
                .prices(routeData.getPrices());
        p.displayData(viewData, directionRes);




    }
}
