package com.rerr.skootardemo.widget.powerup;

import android.os.Build;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by Rerr on 6/29/2015.
 * Description
 * -> describe here
 */
public class SwipeBackPageTransform implements ViewPager.PageTransformer {

    private static final float MIN_SCALE = 0.85f;
    private static final float MIN_ALPHA = 0.5f;

    /**
     * Apply a property transformation to the given page.
     *
     * @param page     Apply the transformation to this page
     * @param position Position of page relative to the current front-and-center
     *                 position of the pager. 0 is front and center. 1 is one full
     */

    public void transformPage(View page, float position) {
        int pageWidth = page.getWidth();
        int pageHeight = page.getHeight();

        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            page.setAlpha(0);
            page.setVisibility(View.GONE);

        }
        else if (position <= 0) { // [-1,0] moving to left page
            // Use the default slide transition when moving to the left page
            if(position == -1){
                page.setVisibility(View.GONE);
            }
            else {
                page.setVisibility(View.VISIBLE);
            }
            page.setAlpha(1 + position);
            page.setTranslationX(pageWidth * -position);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                page.setElevation(0);

            // Scale the page down (between MIN_SCALE and 1)
            float scaleFactor = MIN_SCALE
                    + (1 - MIN_SCALE) * (1 - Math.abs(position));
            page.setScaleX(scaleFactor);
            page.setScaleY(scaleFactor);


        }
        else if (position <= 1) { // (0,1] moving to right page
            // Use the default slide transition when moving to the right page
            page.setVisibility(View.VISIBLE);
            page.setAlpha(1);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                page.setElevation(1);

            // Counteract the default slide transition
//            page.setTranslationX(pageWidth * -position);
            page.setScaleX(1);
            page.setScaleY(1);
        }
        else { // (1,+Infinity]
            // This page is way off-screen to the right.
            page.setAlpha(0);
            page.setVisibility(View.GONE);
        }
    }
}
