package com.rerr.skootardemo.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created by Rerr on 1/19/2015.
 */
public class HttpUtil {
    public static int postConnect(String urlServer, String jsonContent, StringBuilder returnData)
            throws IOException {

        int responseCode = 0;
        URL url = new URL(urlServer);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        connection.setRequestProperty("Accept-Encoding", "gzip,deflate");
        connection.setRequestProperty("Content-Length", Integer.toString(jsonContent.getBytes().length));
        connection.setRequestMethod("POST");
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);

        OutputStream out = connection.getOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");
        writer.write(jsonContent);
        writer.flush();
        writer.close();

        if (isRedirect(connection)) {
            connection = Redirect(connection, jsonContent);
        }

        responseCode = connection.getResponseCode();
        InputStream is = connection.getInputStream();

        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        if (returnData != null) {
            StringBuilder serverRet = returnData;
            String line;
            while ((line = r.readLine()) != null) {
                serverRet.append(line);
            }
        }

        r.close();

        return responseCode;
    }

    private static HttpURLConnection Redirect(HttpURLConnection connection, String jsonContent) throws IOException {
        String newUrl = connection.getHeaderField("Location");
        String requestMethod = connection.getRequestMethod();
        // get the cookie if need, for login
        String cookies = connection.getHeaderField("Set-Cookie");

        connection.disconnect();

        HttpURLConnection retConn = (HttpURLConnection) new URL(newUrl).openConnection();
        if (requestMethod.equals("POST")) {
            retConn.setDoOutput(true);
            retConn.setRequestProperty("Cookie", cookies);
            retConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            retConn.setRequestProperty("Accept-Encoding", "gzip,deflate");
            retConn.setRequestProperty("Content-Length", Integer.toString(jsonContent.getBytes().length));
            retConn.setRequestMethod("POST");
            retConn.setConnectTimeout(5000);
            retConn.setReadTimeout(5000);
        }

        return retConn;
    }

    private static boolean isRedirect(HttpURLConnection connection) throws IOException {
        boolean redirect = false;
        int respCode = connection.getResponseCode();
        if (respCode != HttpURLConnection.HTTP_OK &&
                respCode != HttpURLConnection.HTTP_NO_CONTENT) {

            if (respCode == HttpURLConnection.HTTP_MOVED_TEMP
                    || respCode == HttpURLConnection.HTTP_MOVED_PERM
                    || respCode == HttpURLConnection.HTTP_SEE_OTHER)
                redirect = true;
        }
        return redirect;
    }

    public static CharSequence uploadFile(String urlServer, String sourceFileUri, String fileName, String jsonDataStr) {

//        String fileName = sourceFileUri;

        HttpURLConnection connection = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        int serverResponseCode = 0;

        if (!sourceFile.isFile()) {
            return null;

        } else {
            try {

                // compress bitmap
//                BitmapFactory.Options options = new BitmapFactory.Options();
//                options.inJustDecodeBounds = true;
//                options.inSampleSize = 1;
//                BitmapFactory.decodeFile(sourceFileUri, options);
//                int bytesRequest = options.outWidth * options.outHeight;
//                if (bytesRequest > maxBufferSize){
//                    float fSample = (float)bytesRequest / (float)maxBufferSize;
//                    int sample = Math.round(fSample);
//                    options.inSampleSize = sample;
//                }
//                options.inJustDecodeBounds = false;
//                Bitmap bmp = BitmapFactory.decodeFile(sourceFileUri, options);
                Bitmap bmp = BitmapFactory.decodeFile(sourceFileUri);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 80, bos);

                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(urlServer);

                // Open a HTTP  connection to  the URL
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true); // Allow Inputs
                connection.setDoOutput(true); // Allow Outputs
                connection.setUseCaches(false); // Don't use a Cached Copy
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
//                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                dos = new DataOutputStream(connection.getOutputStream());

                // json data part
                if(jsonDataStr != null && !jsonDataStr.isEmpty()) {

                    // start boundary json data part
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes(
                            "Content-Disposition: form-data; name=\"upload_info\"" + lineEnd +
                                    "Content-Type: application/json" + lineEnd
                    );

                    dos.writeBytes(lineEnd);

                    dos.writeBytes(jsonDataStr);

                    // end json data part
                    dos.writeBytes(lineEnd);
                }

                // start boundary file part
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes(
                        "Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" +
                        fileName + "\"" + lineEnd +
                        "Content-Type: image/jpeg" + lineEnd +
                        "Content-Transfer-Encoding: base64" + lineEnd
                );

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = bos.toByteArray().length;

                bufferSize = bytesAvailable/*Math.min(bytesAvailable, maxBufferSize)*/;
                buffer = bos.toByteArray();

                // read file and write it into form...
                dos.write(buffer, 0, bufferSize);

                // send multipart form data necesssary after file data...
                // end file data part
                dos.writeBytes(lineEnd);

                // end multi-part form
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = connection.getResponseCode();
                String serverResponseMessage = connection.getResponseMessage();

                if (isRedirect(connection)) {
                    String newUrl = connection.getHeaderField("Location");
                    String requestMethod = connection.getRequestMethod();
                    // get the cookie if need, for login
                    String cookies = connection.getHeaderField("Set-Cookie");

                    connection.disconnect();
                    dos.flush();
                    dos.close();

                    connection = (HttpURLConnection) new URL(newUrl).openConnection();

                    connection.setDoInput(true); // Allow Inputs
                    connection.setDoOutput(true); // Allow Outputs
                    connection.setUseCaches(false); // Don't use a Cached Copy
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Connection", "Keep-Alive");
//                    connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                    connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    dos = new DataOutputStream(connection.getOutputStream());

                    // json data part
                    if(jsonDataStr != null && !jsonDataStr.isEmpty()) {

                        // start boundary json data part
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes(
                                "Content-Disposition: form-data; name=\"upload_info\"" + lineEnd +
                                "Content-Type: application/json" + lineEnd
                        );

                        dos.writeBytes(lineEnd);

                        dos.writeBytes(jsonDataStr);

                        // end json data part
                        dos.writeBytes(lineEnd);
                    }

                    // start boundary file part
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes(
                            "Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" +
                            fileName + "\"" + lineEnd +
                            "Content-Type: image/jpeg" + lineEnd +
                            "Content-Transfer-Encoding: base64" + lineEnd
                    );

                    dos.writeBytes(lineEnd);

                    // create a buffer of  maximum size
                    bytesAvailable = bos.toByteArray().length;

                    bufferSize = bytesAvailable/*Math.min(bytesAvailable, maxBufferSize)*/;
                    buffer = bos.toByteArray();

                    // read file and write it into form...
                    dos.write(buffer, 0, bufferSize);

                    // send multipart form data necesssary after file data...
                    // end file data part
                    dos.writeBytes(lineEnd);

                    // end multi-part form
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    serverResponseCode = connection.getResponseCode();
                    serverResponseMessage = connection.getResponseMessage();
                }

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                InputStream is = connection.getInputStream();
                BufferedReader r = new BufferedReader(new InputStreamReader(is));
                StringBuilder serverRet = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    serverRet.append(line);
                }

                r.close();

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

                return serverRet;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.e("Upload file to server", "error: " + e.getMessage(), e);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Upload file to server", "error: " + e.getMessage(), e);
            }
            return null;

        } // End else block
    }

}
