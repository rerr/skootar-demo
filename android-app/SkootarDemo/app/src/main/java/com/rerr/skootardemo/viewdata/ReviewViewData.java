package com.rerr.skootardemo.viewdata;

import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;
import com.rerr.skootardemo.base.BeginFragment;
import com.rerr.skootardemo.base.BeginViewData;

/**
 * Created by Rerr on 7/2/2015.
 * Description
 * -> describe here
 */
public class ReviewViewData extends BeginViewData {

    //Cover
    public static final String SENDER_NAME = "sender_name";
    public static final String SENDER_PHONE = "sender_phone";
    public static final String RECEIVER_NAME = "receiver_name";
    public static final String RECEIVER_PHONE = "receiver_phone";

    //Detail
    public static final String FROM_ADDR = "from";
    public static final String TO_ADDR = "to";
    public static final String FROM_LAT_LNG = "from_lat_lng";
    public static final String TO_LAT_LNG= "to_lat_lng";
    public static final String DISTANCE_METERS = "distance_meters";
    public static final String PRICES = "price";

    public static ReviewViewData build(){
        return new ReviewViewData();
    }

    /**
     * Set
     */
    public ReviewViewData fromAddr(String value){
        this.get().putString(FROM_ADDR, value);
        return this;
    }
    public ReviewViewData toAddr(String value){
        this.get().putString(TO_ADDR, value);
        return this;
    }
    public ReviewViewData fromLatLng(LatLng latLng){
        double[] lat_lng = new double[]{latLng.latitude, latLng.longitude};
        this.get().putDoubleArray(FROM_LAT_LNG, lat_lng);
        return this;
    }
    public ReviewViewData toLatLng(LatLng latLng){
        double[] lat_lng = new double[]{latLng.latitude, latLng.longitude};
        this.get().putDoubleArray(TO_LAT_LNG, lat_lng);
        return this;
    }
    public ReviewViewData distanceMeters(int value){
        this.get().putInt(DISTANCE_METERS, value);
        return this;
    }
    public ReviewViewData prices(float value){
        this.get().putFloat(PRICES, value);
        return this;
    }

    /**
     * Set
     */
    public ReviewViewData senderName(String value){
        this.get().putString(SENDER_NAME, value);
        return this;
    }
    public ReviewViewData senderPhone(String value){
        this.get().putString(SENDER_PHONE, value);
        return this;
    }
    public ReviewViewData receiverName(String value){
        this.get().putString(RECEIVER_NAME, value);
        return this;
    }
    public ReviewViewData receiverPhone(String value){
        this.get().putString(RECEIVER_PHONE, value);
        return this;
    }

    /**
     * Get
     */
    public String getSenderName(){
        return this.get() == null ? null : this.get().getString(SENDER_NAME);
    }
    public String getSenderPhone(){
        return this.get() == null ? null : this.get().getString(SENDER_PHONE);
    }
    public String getReceiverName(){
        return this.get() == null ? null : this.get().getString(RECEIVER_NAME);
    }
    public String getReceiverPhone(){
        return this.get() == null ? null : this.get().getString(RECEIVER_PHONE);
    }

    /**
     * Get
     */
    public String getFromAddr(){
        return this.get() == null ? null : this.get().getString(FROM_ADDR);
    }
    public String getToAddr(){
        return this.get() == null ? null : this.get().getString(TO_ADDR);
    }
    public LatLng getFromLatLng(){
        if(this.get() == null)
            return null;
        double[] lat_lng = this.get().getDoubleArray(FROM_LAT_LNG);
        return new LatLng(lat_lng[0], lat_lng[1]);
    }
    public LatLng getToLatLng(){
        if(this.get() == null)
            return null;
        double[] lat_lng = this.get().getDoubleArray(TO_LAT_LNG);
        return new LatLng(lat_lng[0], lat_lng[1]);
    }
    public int getDistanceMeters(){
        return this.get() == null ? 0 : this.get().getInt(DISTANCE_METERS);
    }
    public float getPrices(){
        return this.get() == null ? 0 : this.get().getFloat(PRICES);
    }

}
