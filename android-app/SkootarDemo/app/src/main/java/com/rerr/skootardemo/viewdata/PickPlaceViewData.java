package com.rerr.skootardemo.viewdata;

import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;
import com.rerr.skootardemo.base.BeginViewData;

/**
 * Created by Rerr on 7/2/2015.
 * Description
 * -> describe here
 */
public class PickPlaceViewData extends BeginViewData {

    public static final String PLACE_NAME = "place_name";
    public static final String LAT_LNG = "lat_lng";
    public static final String ADDRESS = "address";
    public static final String PHONE = "phone";

    public static PickPlaceViewData build(){
        return new PickPlaceViewData();
    }

    /**
     * Set
     */
    public PickPlaceViewData placeName(String name){
        this.get().putString(PLACE_NAME, name);
        return this;
    }
    public PickPlaceViewData placeLatLng(LatLng latLng){
        double[] lat_lng = new double[]{latLng.latitude, latLng.longitude};
        this.get().putDoubleArray(LAT_LNG, lat_lng);
        return this;
    }
    public PickPlaceViewData placeAddress(String address){
        this.get().putString(ADDRESS, address);
        return this;
    }
    public PickPlaceViewData placePhone(String phone){
        this.get().putString(PHONE, phone);
        return this;
    }

    /**
     * Get
     */
    public String getPlaceName(){
        return this.get() == null ? null : this.get().getString(PLACE_NAME);
    }
    public String getPlaceAddress(){
        return this.get() == null ? null : this.get().getString(ADDRESS);
    }

    public LatLng getPlaceLatLng(){
        if(this.get() == null)
            return null;
        double[] lat_lng = this.get().getDoubleArray(LAT_LNG);
        return new LatLng(lat_lng[0], lat_lng[1]);
    }

    public String getPlacePhone(){
        return this.get() == null ? null : this.get().getString(PHONE);
    }


}
