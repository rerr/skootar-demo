package com.rerr.skootardemo;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rerr.skootardemo.base.BeginGoogleMapFragment;
import com.rerr.skootardemo.controller.ReviewController;
import com.rerr.skootardemo.helper.DirectionAwesome;
import com.rerr.skootardemo.helper.MapAwesome;
import com.rerr.skootardemo.viewdata.ReviewViewData;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewFragment extends BeginGoogleMapFragment {

    private static final String TAG = ReviewFragment.class.getSimpleName();
    private static final String VIEW_DATA = "view_data";

    private Controller mController;
    private ReviewViewData mData;

    /**
     * Toolbar
     */
    private ImageView mBackNav;
    private TextView mPageTitle;

    /**
     * Map
     */
    private TextView mMapMsgBox;
    private MarkerOptions mMarkerOrigin;
    private MarkerOptions mMarkerDestination;

    /**
     * Views
     */
    private TextView mSenderName;
    private TextView mSenderPhone;
    private TextView mReceiverName;
    private TextView mReceiverPhone;
    private TextView mFromAddr;
    private TextView mToAddr;
    private TextView mDistance;
    private TextView mPrices;

    public static ReviewFragment createInstance(){
        ReviewFragment p = new ReviewFragment();
        p.setController(new ReviewController());
        return p;
    }

    private void setController(Controller controller){
        mController = controller;
    }

    public void setData(ReviewViewData data){
        mData = data;
    }

    public ReviewFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutViewId() {
        return R.layout.fragment_review;
    }

    @Override
    protected MapView getMapViewObject(View contentView) {
        View embeddedMap = contentView.findViewById(R.id.app_embedded_map);
        mMapMsgBox = (TextView) embeddedMap.findViewById(R.id.map_msg_box);
        mMapMsgBox.setText("-");
        return mMapObj = (MapView) embeddedMap.findViewById(R.id.map);
    }

    @Override
    protected void setUpMapUi() {
//        super.setUpMapUi();
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        mMap.setMyLocationEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
    }

    @Override
    protected void onMapLoaded(GoogleMap map) {
        mMarkerOrigin = MapAwesome.buildMarkerOption(getResources(), R.drawable.ic_marker_origin);
        mMarkerDestination = MapAwesome.buildMarkerOption(getResources(), R.drawable.ic_marker_destination);
    }

    @Override
    protected void initSaveViewState(Bundle savedInstanceState) {
        if(savedInstanceState != null){
            if(mData == null)
                mData = new ReviewViewData();
            mData.set(savedInstanceState.getBundle(VIEW_DATA));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    protected void initToolbar(View v) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.app_toolbar).findViewById(R.id.awesome_toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);

        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        ActionBar ab = activity.getSupportActionBar();
        assert ab != null;
        ab.setCustomView(R.layout.cab_common_nav_toolbar);
        ab.setDisplayShowCustomEnabled(true);
        View cabView = ab.getCustomView();
        mPageTitle = (TextView) cabView.findViewById(R.id.title);
        mPageTitle.setText("Review");
        mBackNav = (ImageView) cabView.findViewById(R.id.back_nav);
        mBackNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    protected void init(View v) {
        mSenderName = (TextView) v.findViewById(R.id.review_tv_sender_name);
        mSenderPhone = (TextView) v.findViewById(R.id.review_tv_sender_tel);
        mReceiverName = (TextView) v.findViewById(R.id.review_tv_recv_name);
        mReceiverPhone = (TextView) v.findViewById(R.id.review_tv_recv_tel);
        mFromAddr = (TextView) v.findViewById(R.id.review_tv_sender_addr);
        mToAddr = (TextView) v.findViewById(R.id.review_tv_recv_addr);
        mDistance = (TextView) v.findViewById(R.id.review_tv_pay_distance);
        mPrices = (TextView) v.findViewById(R.id.review_tv_pay_prices);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(mData != null) {
            outState.putBundle(VIEW_DATA, mData.get());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void displayData(final ReviewViewData data, final DirectionAwesome.DecodeResult directionRes){
        mData = data;
        mSenderName.setText(mData.getSenderName());
        mSenderPhone.setText(mData.getSenderPhone());
        mReceiverName.setText(mData.getReceiverName());
        mReceiverPhone.setText(mData.getReceiverPhone());
        mFromAddr.setText(mData.getFromAddr());
        mToAddr.setText(mData.getToAddr());
        mDistance.setText(String.format("%.1f km.", (float) mData.getDistanceMeters() / 1000.0f));
        mPrices.setText(String.format("%d Baht.", Math.round(mData.getPrices())));
        mMapMsgBox.setText(String.format("%.1f km.", (float) mData.getDistanceMeters() / 1000.0f));


        mMap.clear();
        MapAwesome.drawCustomMarker(mMap, data.getFromLatLng(), mMarkerOrigin);
        MapAwesome.drawCustomMarker(mMap, data.getToLatLng(), mMarkerDestination);
        MapAwesome.drawRoutePolyline(mMap, directionRes.getOverviewPolyline());
        MapAwesome.animateToFitBounds(mMap, directionRes.getBounds());
    }

    public interface Controller{
        void onSubmitClick(View sender, ReviewViewData data);
    }

}
