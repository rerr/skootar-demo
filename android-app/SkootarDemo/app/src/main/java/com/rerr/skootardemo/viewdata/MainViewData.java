package com.rerr.skootardemo.viewdata;

import android.os.Bundle;

import com.rerr.skootardemo.base.BeginViewData;

/**
 * Created by Rerr on 7/2/2015.
 * Description
 * -> describe here
 */
public class MainViewData extends BeginViewData {

    public static final String SENDER_NAME = "sender_name";
    public static final String SENDER_TEL = "sender_tel";
    public static final String RECEIVER_NAME = "receiver_name";
    public static final String RECEIVER_TEL = "receiver_tel";

}
