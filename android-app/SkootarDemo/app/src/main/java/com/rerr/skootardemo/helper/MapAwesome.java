package com.rerr.skootardemo.helper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.browse.MediaBrowser;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.SphericalUtil;
import com.rerr.skootardemo.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Rerr on 7/10/2015.
 * Description
 * -> describe here
 */
public class MapAwesome {

    public static void showMapView(View view){
        view.setAlpha(1);
        view.setVisibility(View.VISIBLE);
    }

    public static void hideMapView(View view){
        view.setAlpha(0);
        view.setVisibility(View.INVISIBLE);
    }

    public static void fadeInMapView(View view){
        view.setVisibility(View.VISIBLE);
        view.animate().alpha(1)
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(500)
                .start();
    }

    public static void fadeOutMapView(View view){
        view.setVisibility(View.INVISIBLE);
        view.animate().alpha(0)
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(500)
                .start();
    }

    public static void animateMapTo(final GoogleMap map, final LatLng destination){
        MarkerOptions mop = new MarkerOptions()
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                                .position(destination);
        animateMapTo(map, destination, mop);

    }

    public static void animateMapTo(final GoogleMap map, final LatLng destination, final MarkerOptions customMarker){

        customMarker.position(destination);
        double distance  = SphericalUtil.computeDistanceBetween(map.getCameraPosition().target, destination);
        if(distance < 100 * 1000){
            CameraPosition curr = new CameraPosition.Builder()
                    .target(destination)
                    .bearing(0)
                    .tilt(90)
                    .zoom(map.getMaxZoomLevel() / 1.5f)
                    .build();
            CameraUpdate newCam = CameraUpdateFactory.newCameraPosition(curr);
            map.animateCamera(newCam);
            map.clear();
            map.addMarker(customMarker);
            return;
        }

        CameraPosition curr = new CameraPosition.Builder()
                .target(destination)
                .bearing(45)
                .tilt(0)
                .zoom(map.getCameraPosition().zoom / 1.5f)
                .build();
        CameraUpdate newCam = CameraUpdateFactory.newCameraPosition(curr);
        map.animateCamera(newCam, 2500, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                CameraPosition curr = new CameraPosition.Builder()
                        .target(destination)
                        .bearing(0)
                        .tilt(90)
                        .zoom(map.getMaxZoomLevel() / 1.5f)
                        .build();
                CameraUpdate newCam = CameraUpdateFactory.newCameraPosition(curr);
                map.animateCamera(newCam, 2500, null);
                map.clear();
                map.addMarker(customMarker);
            }

            @Override
            public void onCancel() {

            }
        });
    }

    public static void animateToFitBounds(final GoogleMap map, final List<LatLng> latLngList){
        LatLngBounds.Builder builder = LatLngBounds.builder();
        LatLngBounds latLngBounds = null;
        for(LatLng latLng : latLngList){
            builder.include(latLng);
        }
        latLngBounds = builder.build();
        MapAwesome.animateToFitBounds(map, latLngBounds);

    }

    public static void moveToFitBounds(final GoogleMap map, final List<LatLng> latLngList){
        LatLngBounds.Builder builder = LatLngBounds.builder();
        LatLngBounds latLngBounds = null;
        for(LatLng latLng : latLngList){
            builder.include(latLng);
        }
        latLngBounds = builder.build();
        MapAwesome.moveToFitBounds(map, latLngBounds);

    }

    public static void animateToFitBounds(final GoogleMap map, final LatLngBounds bounds) {
        CameraUpdate newCam = CameraUpdateFactory.newLatLngBounds(bounds, 170);
        map.animateCamera(newCam, 2500, null);
    }

    public static void moveToFitBounds(final GoogleMap map, final LatLngBounds bounds) {
        CameraUpdate newCam = CameraUpdateFactory.newLatLngBounds(bounds, 170);
        map.moveCamera(newCam);
    }

    public static void moveMapTo(final GoogleMap map, LatLng destination){
        MarkerOptions mop = new MarkerOptions()
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                        .position(destination);
        moveMapTo(map, destination, mop);
    }

    public static void moveMapTo(final GoogleMap map, LatLng destination, MarkerOptions customMarker){
        CameraPosition curr = new CameraPosition.Builder()
                .target(destination)
                .bearing(0)
                .tilt(90)
                .zoom(map.getMaxZoomLevel() / 1.5f)
                .build();
        CameraUpdate newCam = CameraUpdateFactory.newCameraPosition(curr);
        map.moveCamera(newCam);
        map.clear();
        customMarker.position(destination);
        map.addMarker(customMarker);
    }

    public static void setupUi(final GoogleMap map){
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setMyLocationEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setMapToolbarEnabled(true);
    }

    public static GoogleApiClient setupClientIfNeed(
            GoogleApiClient awesomeClient,
            Context context,
            GoogleApiClient.ConnectionCallbacks callbacks)
    {

        if (awesomeClient == null) {
            awesomeClient =
                    new GoogleApiClient.Builder(context)
                            .addApi(LocationServices.API)
                            .addApi(Places.GEO_DATA_API)
                            .addApi(Places.PLACE_DETECTION_API)
                            .addConnectionCallbacks(callbacks)
                            .build();
        }
        if(awesomeClient != null) {
            if(!awesomeClient.isConnected())
                awesomeClient.connect();
        }
        return awesomeClient;
    }

    public static void freeMap(final GoogleMap map){
        map.setMyLocationEnabled(false);
    }

    public static void drawRoutePolyline(GoogleMap map, String encodedPolyline){
        if(encodedPolyline == null)
            return;

        List<LatLng> pathPoints = PolyUtil.decode(encodedPolyline);
        PolylineOptions pop = new PolylineOptions().geodesic(true);
        pop.color(Color.parseColor("#ff00a091"));
        pop.width(15);
        for(LatLng p : pathPoints){
            pop.add(p);
        }
        map.addPolyline(pop);
    }

    public static void drawMarkers(GoogleMap map, List<LatLng> points){
        for(LatLng latLng : points){
            map.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                    .position(latLng));
        }
    }

    public static void drawCustomMarker(GoogleMap map, LatLng points, MarkerOptions markerOptions){
        markerOptions.position(points);
            map.addMarker(markerOptions);
    }

    public static MarkerOptions buildMarkerOption(Resources resources, int resId){
        Bitmap icon = BitmapFactory.decodeResource(resources, resId);
        MarkerOptions mop = new MarkerOptions()
                .position(new LatLng(0, 0))
                .icon(BitmapDescriptorFactory.fromBitmap(icon));
        return mop;
    }


}
