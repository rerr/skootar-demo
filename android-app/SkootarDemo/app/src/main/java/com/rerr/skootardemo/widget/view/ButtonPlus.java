package com.rerr.skootardemo.widget.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;

import com.rerr.skootardemo.R;
import com.rerr.skootardemo.widget.helper.TypeFaces;

/**
 * Created by Rerr on 6/15/2015.
 * Description
 * TextViewPlus is
 */
public class ButtonPlus extends Button {

    private String mTypeface;
    private Context mContext;

    public ButtonPlus(Context context) {
        this(context, null);
    }

    public ButtonPlus(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.buttonStyle);
    }

    public ButtonPlus(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initAttrs(attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ButtonPlus(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        initAttrs(attrs, defStyleAttr);
    }

    private void initAttrs(AttributeSet attrs, int defStyle){

        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.TextPlus, defStyle, 0);

        mTypeface = a.getString(R.styleable.TextPlus_typefaceName);
        if(mTypeface != null){
            Typeface tf = TypeFaces.getTypeFace(mContext, mTypeface);
            if(tf != null)
                this.setTypeface(tf);
        }
        a.recycle();
    }
}
