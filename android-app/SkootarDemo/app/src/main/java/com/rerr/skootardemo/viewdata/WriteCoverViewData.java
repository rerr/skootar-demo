package com.rerr.skootardemo.viewdata;

import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;
import com.rerr.skootardemo.base.BeginViewData;

/**
 * Created by Rerr on 7/2/2015.
 * Description
 * -> describe here
 */
public class WriteCoverViewData extends BeginViewData {

    public static final String SENDER_NAME = "sender_name";
    public static final String SENDER_PHONE = "sender_phone";
    public static final String RECEIVER_NAME = "receiver_name";
    public static final String RECEIVER_PHONE = "receiver_phone";

    public static WriteCoverViewData build(){
        return new WriteCoverViewData();
    }

    /**
     * Set
     */
    public WriteCoverViewData senderName(String value){
        this.get().putString(SENDER_NAME, value);
        return this;
    }
    public WriteCoverViewData senderPhone(String value){
        this.get().putString(SENDER_PHONE, value);
        return this;
    }
    public WriteCoverViewData receiverName(String value){
        this.get().putString(RECEIVER_NAME, value);
        return this;
    }
    public WriteCoverViewData receiverPhone(String value){
        this.get().putString(RECEIVER_PHONE, value);
        return this;
    }

    /**
     * Get
     */
    public String getSenderName(){
        return this.get() == null ? null : this.get().getString(SENDER_NAME);
    }
    public String getSenderPhone(){
        return this.get() == null ? null : this.get().getString(SENDER_PHONE);
    }
    public String getReceiverName(){
        return this.get() == null ? null : this.get().getString(RECEIVER_NAME);
    }
    public String getReceiverPhone(){
        return this.get() == null ? null : this.get().getString(RECEIVER_PHONE);
    }

}
