package com.rerr.skootardemo.base;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by Rerr on 6/17/2015.
 * Description
 * -> describe here
 */
public abstract class BeginAppCompatActivity extends AppCompatActivity
{

    public static final String IGNORE_OVERRIDE_OPEN_TRANSITION = "ignore_open_trans";
    public static final String IGNORE_OVERRIDE_PAUSED_TRANSITION = "ignore_paused_trans";

    private boolean mIgnorePausedTransition = false;
    private boolean mIgnoreOpenTransition = false;

    protected abstract void initSaveViewState(Bundle savedInstanceState);
    protected abstract void initToolbar();
    protected abstract void init();

    protected void setIgnorePausedTransition(boolean isIgnore){
        mIgnorePausedTransition = isIgnore;
    }

    protected void setIgnoreOpenTransition(boolean isIgnore){
        mIgnoreOpenTransition = isIgnore;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(IGNORE_OVERRIDE_OPEN_TRANSITION, mIgnoreOpenTransition);
        outState.putBoolean(IGNORE_OVERRIDE_PAUSED_TRANSITION, mIgnorePausedTransition);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if(savedInstanceState != null){
            mIgnoreOpenTransition = savedInstanceState.getBoolean(IGNORE_OVERRIDE_OPEN_TRANSITION);
            mIgnorePausedTransition = savedInstanceState.getBoolean(IGNORE_OVERRIDE_PAUSED_TRANSITION);
        }
        else if(intent != null){
            mIgnoreOpenTransition = intent.hasExtra(IGNORE_OVERRIDE_OPEN_TRANSITION) &&
                    intent.getBooleanExtra(IGNORE_OVERRIDE_OPEN_TRANSITION, false);
            mIgnorePausedTransition = intent.hasExtra(IGNORE_OVERRIDE_PAUSED_TRANSITION) &&
                    intent.getBooleanExtra(IGNORE_OVERRIDE_PAUSED_TRANSITION, false);
        }

        if(!mIgnoreOpenTransition) {
            overridePendingTransition(com.rerr.simplelib.R.anim.activity_open_translate, com.rerr.simplelib.R.anim.activity_close_scale);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(!mIgnorePausedTransition) {
            overridePendingTransition(com.rerr.simplelib.R.anim.activity_open_scale, com.rerr.simplelib.R.anim.activity_close_translate);
        }
    }
}
