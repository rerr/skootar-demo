package com.rerr.skootardemo.viewdata;

import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;
import com.rerr.skootardemo.base.BeginViewData;

/**
 * Created by Rerr on 7/2/2015.
 * Description
 * -> describe here
 */
public class SelectRouteViewData extends BeginViewData {

    public static final String FROM_ADDR = "from";
    public static final String TO_ADDR = "to";
    public static final String FROM_LAT_LNG = "from_lat_lng";
    public static final String TO_LAT_LNG= "to_lat_lng";
    public static final String DISTANCE_METERS = "distance_meters";
    public static final String PRICES = "price";

    public static SelectRouteViewData build(){
        return new SelectRouteViewData();
    }

    /**
     * Set
     */
    public SelectRouteViewData fromAddr(String value){
        this.get().putString(FROM_ADDR, value);
        return this;
    }
    public SelectRouteViewData toAddr(String value){
        this.get().putString(TO_ADDR, value);
        return this;
    }
    public SelectRouteViewData fromLatLng(LatLng latLng){
        double[] lat_lng = new double[]{latLng.latitude, latLng.longitude};
        this.get().putDoubleArray(FROM_LAT_LNG, lat_lng);
        return this;
    }
    public SelectRouteViewData toLatLng(LatLng latLng){
        double[] lat_lng = new double[]{latLng.latitude, latLng.longitude};
        this.get().putDoubleArray(TO_LAT_LNG, lat_lng);
        return this;
    }
    public SelectRouteViewData distanceMeters(int value){
        this.get().putInt(DISTANCE_METERS, value);
        return this;
    }
    public SelectRouteViewData prices(float value){
        this.get().putFloat(PRICES, value);
        return this;
    }

    /**
     * Get
     */
    public String getFromAddr(){
        return this.get() == null ? null : this.get().getString(FROM_ADDR);
    }
    public String getToAddr(){
        return this.get() == null ? null : this.get().getString(TO_ADDR);
    }
    public LatLng getFromLatLng(){
        if(this.get() == null)
            return null;
        double[] lat_lng = this.get().getDoubleArray(FROM_LAT_LNG);
        return new LatLng(lat_lng[0], lat_lng[1]);
    }
    public LatLng getToLatLng(){
        if(this.get() == null)
            return null;
        double[] lat_lng = this.get().getDoubleArray(TO_LAT_LNG);
        return new LatLng(lat_lng[0], lat_lng[1]);
    }
    public int getDistanceMeters(){
        return this.get() == null ? 0 : this.get().getInt(DISTANCE_METERS);
    }
    public float getPrices(){
        return this.get() == null ? 0 : this.get().getFloat(PRICES);
    }

}
