package com.rerr.skootardemo;


import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rerr.skootardemo.base.BeginGoogleMapFragment;
import com.rerr.skootardemo.controller.SelectRouteController;
import com.rerr.skootardemo.helper.DirectionAwesome;
import com.rerr.skootardemo.helper.MapAwesome;
import com.rerr.skootardemo.viewdata.PickPlaceViewData;
import com.rerr.skootardemo.viewdata.SelectRouteViewData;
import com.rerr.skootardemo.viewdata.WriteCoverViewData;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SelectRouteFragment extends BeginGoogleMapFragment {

    private static final String TAG = SelectRouteFragment.class.getSimpleName();
    private static final String VIEW_DATA = "view_data";
    private static final String COVER_VIEW_DATA = "cover_view_data";

    /**
     * Toolbar
     */
    private ImageView mBackNav;
    private TextView mPageTitle;

    /**
     * Flag to track is selected mOrigin and mDestination
     */
    private boolean mHasOrigin = false;
    private boolean mHasDestination = false;

    /**
     * Last Pick place data
     */
    private PickPlaceViewData mLastSrcPicked;
    private PickPlaceViewData mLastDstPicked;

    /**
     * Calculate value
     */
    private int mDistanceMeters = 0;
    private float mPrices = 0;
    private DirectionAwesome.DecodeResult mDirectionResult;

    /**
     * Request pick place
     */
    public static final int REQUEST_PICK_SOURCE = 0;
    public static final int REQUEST_PICK_DESTINATION = 1;

    /**
     * Controller and Data
     */
    private Controller mController;
    private SelectRouteViewData mData;

    /**
     * Cover View Data
     */
    private WriteCoverViewData mCoverData;

    /**
     * Map
     */
    private TextView mMapMsgBox;

    private TextView mFromSource;
    private TextView mToDestination;
    private Button mBtnOk;
    private TextView mCalPrices;
    private MarkerOptions mMarkerOrigin;
    private MarkerOptions mMarkerDestination;

    public static SelectRouteFragment createInstance(){
        SelectRouteFragment p = new SelectRouteFragment();
        p.setController(new SelectRouteController());
        return p;
    }

    public void setController(Controller controller){
        mController = controller;
    }

    public void setData(SelectRouteViewData data){
        mData = data;
    }

    public void setCoverViewData(WriteCoverViewData data){
        mCoverData = data;
    }

    public SelectRouteFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutViewId() {
        return R.layout.fragment_select_route;
    }

    @Override
    protected MapView getMapViewObject(View contentView) {
        View embeddedMap = contentView.findViewById(R.id.app_embedded_map);
        mMapMsgBox = (TextView) embeddedMap.findViewById(R.id.map_msg_box);
        mMapMsgBox.setText("-");
        return mMapObj = (MapView) embeddedMap.findViewById(R.id.map);
    }

    @Override
    protected void initSaveViewState(Bundle savedInstanceState) {
        if(savedInstanceState != null){
            if(mData == null)
                mData = SelectRouteViewData.build();
            mData.set(savedInstanceState.getBundle(VIEW_DATA));
            if(mCoverData == null)
                mCoverData = WriteCoverViewData.build();
            mCoverData.set(savedInstanceState.getBundle(COVER_VIEW_DATA));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    protected void initToolbar(View v) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.app_toolbar).findViewById(R.id.awesome_toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);

        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        ActionBar ab = activity.getSupportActionBar();
        assert ab != null;
        ab.setCustomView(R.layout.cab_common_nav_toolbar);
        ab.setDisplayShowCustomEnabled(true);
        View cabView = ab.getCustomView();
        mPageTitle = (TextView) cabView.findViewById(R.id.title);
        mPageTitle.setText("Select Routes");
        mBackNav = (ImageView) cabView.findViewById(R.id.back_nav);
        mBackNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    protected void init(View v) {

        final Fragment self = this;

        View routeSel = v.findViewById(R.id.sel_route_cl_form_route_selector);
        mFromSource = (TextView) routeSel.findViewById(R.id.from_source);
        mFromSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickPlaceActivity.startForResult(self, mLastSrcPicked,
                        SelectRouteFragment.REQUEST_PICK_SOURCE);
            }
        });

        mToDestination = (TextView) routeSel.findViewById(R.id.to_destination);
        mToDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickPlaceActivity.startForResult(self, mLastDstPicked,
                        SelectRouteFragment.REQUEST_PICK_DESTINATION);
            }
        });

        mBtnOk = (Button) v.findViewById(R.id.sel_route_btn_ok);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isReadyData()) {
                    if (mController != null) {
                        mController.onOkClick(view, mData, mCoverData, mDirectionResult);
                    }
                } else {
                    Toast.makeText(getActivity(),
                            "Please pick origin and destination.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        mCalPrices = (TextView) v.findViewById(R.id.sel_route_tv_cal_prices);
        mCalPrices.setText("0 Baht.");
    }

    private boolean isReadyData() {
        boolean ready = mLastDstPicked != null && mLastSrcPicked != null;
        return ready;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(mData == null)
            mData = SelectRouteViewData.build();
        if(mCoverData == null)
            mCoverData = WriteCoverViewData.build();

        outState.putBundle(VIEW_DATA, mData.get());
        outState.putBundle(COVER_VIEW_DATA, mCoverData.get());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void setUpMapUi() {
//        super.setUpMapUi();
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        mMap.setMyLocationEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
    }

    @Override
    protected void onMapLoaded(GoogleMap map) {
        mMarkerOrigin = MapAwesome.buildMarkerOption(getResources(), R.drawable.ic_marker_origin);
        mMarkerDestination = MapAwesome.buildMarkerOption(getResources(), R.drawable.ic_marker_destination);
    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_PICK_SOURCE && resultCode == Activity.RESULT_OK){
            String key = PickPlaceViewData.class.getSimpleName();
            if(data.hasExtra(key)){
                mLastSrcPicked = PickPlaceViewData.build();
                mLastSrcPicked.set(data.getBundleExtra(key));
                mFromSource.setText(mLastSrcPicked.getPlaceName());
            }

            if(mLastSrcPicked != null){
                mHasOrigin = true;
            }

            if(mHasOrigin && mHasDestination){
                calculateAndDisplayFinalResult();
            }
            else{
                if(mLastSrcPicked != null)
                    MapAwesome.moveMapTo(mMap, mLastSrcPicked.getPlaceLatLng(), mMarkerOrigin);
            }
        }
        else if(requestCode == REQUEST_PICK_DESTINATION && resultCode == Activity.RESULT_OK){
            String key = PickPlaceViewData.class.getSimpleName();
            if(data.hasExtra(key)){
                mLastDstPicked = PickPlaceViewData.build();
                mLastDstPicked.set(data.getBundleExtra(key));
                mToDestination.setText(mLastDstPicked.getPlaceName());
            }

            if(mLastDstPicked != null){
                mHasDestination = true;
            }

            if(mHasOrigin && mHasDestination){
                calculateAndDisplayFinalResult();
            }
            else{
                if(mLastDstPicked != null)
                    MapAwesome.moveMapTo(mMap, mLastDstPicked.getPlaceLatLng(), mMarkerDestination);
            }
        }
        else{
            Log.d(TAG, "Invalid pick place result");
        }
    }

    private void buildViewData(){
        mData = SelectRouteViewData.build()
                .fromAddr(mLastSrcPicked.getPlaceAddress())
                .fromLatLng(mLastSrcPicked.getPlaceLatLng())
                .toAddr(mLastDstPicked.getPlaceAddress())
                .toLatLng(mLastDstPicked.getPlaceLatLng())
                .distanceMeters(mDistanceMeters)
                .prices(mPrices);
    }

    private void calculateAndDisplayFinalResult(){
        mMap.clear();
        MapAwesome.drawCustomMarker(mMap, mLastSrcPicked.getPlaceLatLng(), mMarkerOrigin);
        MapAwesome.drawCustomMarker(mMap, mLastDstPicked.getPlaceLatLng(), mMarkerDestination);

        DirectionAwesome.RequireParams p = DirectionAwesome.RequireParams.build()
                .origin(String.format("%f, %f", mLastSrcPicked.getPlaceLatLng().latitude, mLastSrcPicked.getPlaceLatLng().longitude))
                .destination(String.format("%f, %f", mLastDstPicked.getPlaceLatLng().latitude, mLastDstPicked.getPlaceLatLng().longitude));
        DirectionAwesome.sendRequest(p, new DirectionAwesome.OnDirectionRequestComplete() {
            @Override
            public void onComplete(DirectionAwesome.DecodeResult result) {
                if(result != null) {
                    mDirectionResult = result;
                    MapAwesome.drawRoutePolyline(mMap, result.getOverviewPolyline());
                    MapAwesome.animateToFitBounds(mMap, result.getBounds());
                    mDistanceMeters = result.getDistanceMeters();
                    float distanceKm = ((float) result.getDistanceMeters())/1000.0f;
                    mPrices = Math.round(distanceKm * 80);
                    mMapMsgBox.setText(String.format("%.1f km.", distanceKm));
                    mCalPrices.setText(String.format("%d Baht.", Math.round(mPrices)));
                    Log.d(TAG, result.toString());

                    buildViewData();
                }
                else{
                    Log.d(TAG, "no result return");
                }
            }
        });
    }

    public interface Controller{
        void onOkClick(View sender,
                       SelectRouteViewData routeData, WriteCoverViewData coverData,
                       DirectionAwesome.DecodeResult directionRes);
    }
}
