package com.rerr.skootardemo.helper;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Rerr on 7/10/2015.
 * Description
 * -> describe here
 */
public class DirectionAwesome {

    public static final String REST_URL = "http://maps.googleapis.com/maps/api/directions/json";

    public static void sendRequest(RequireParams p, final OnDirectionRequestComplete callback){
        JSONObject params = new JSONObject();
        try {
            params.put(RequireParams.ORIGIN, p.mOrigin);
            params.put(RequireParams.DESTINATION, p.mDestination);
            HttpAwesome.getConnectTo(REST_URL, params, new HttpAwesome.OnCompleteCallback() {
                @Override
                public void complete(String result) {
                    if(callback != null){
                        try {
                            JSONObject directionResult = new JSONObject(result);
                            DecodeResult decodeResult = new DecodeResult(directionResult);
                            callback.onComplete(decodeResult);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            callback.onComplete(null);
                        }

                    }
                }
            }).execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static class RequireParams{
        public static final String ORIGIN = "origin";
        public static final String DESTINATION = "destination";
        public String mOrigin;
        public String mDestination;
        public static RequireParams build(){
            return new RequireParams();
        }

        public RequireParams origin(String value){
            this.mOrigin = value;
            return this;
        }

        public RequireParams destination(String value){
            this.mDestination = value;
            return this;
        }
    }

    public interface OnDirectionRequestComplete{
        void onComplete(DecodeResult result);
    }

    public static class DecodeResult {
        private static final String STATUS_OK = "OK";
        private LatLngBounds mBounds;
        private String mOverviewPolyline;
        private int mDistanceMeters;

        public LatLngBounds getBounds() {
            return mBounds;
        }

        public String getOverviewPolyline() {
            return mOverviewPolyline;
        }

        public int getDistanceMeters(){
            return mDistanceMeters;
        }

        public DecodeResult(JSONObject raw){
            try {
                String status = raw.getString("status");
                if(status.equals(STATUS_OK)) {
                    JSONArray routes = raw.getJSONArray("routes");
                    JSONObject val = routes.getJSONObject(0);
                    JSONObject northEastObj = val
                            .getJSONObject("bounds")
                            .getJSONObject("northeast");
                    JSONObject southWestObj = val
                            .getJSONObject("bounds")
                            .getJSONObject("southwest");
                    LatLng northEast = new LatLng(northEastObj.getDouble("lat"), northEastObj.getDouble("lng"));
                    LatLng southWest = new LatLng(southWestObj.getDouble("lat"), southWestObj.getDouble("lng"));
                    mBounds = new LatLngBounds(southWest, northEast);

                    mOverviewPolyline = val
                            .getJSONObject("overview_polyline")
                            .getString("points");

                    mDistanceMeters = val
                            .getJSONArray("legs")
                            .getJSONObject(0)
                            .getJSONObject("distance")
                            .getInt("value");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
