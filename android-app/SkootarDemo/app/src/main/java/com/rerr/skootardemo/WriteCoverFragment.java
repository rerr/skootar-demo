package com.rerr.skootardemo;


import android.content.res.Configuration;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.rerr.skootardemo.base.BeginFragment;
import com.rerr.skootardemo.controller.WriteCoverController;
import com.rerr.skootardemo.viewdata.WriteCoverViewData;


/**
 * A simple {@link Fragment} subclass.
 */
public class WriteCoverFragment extends BeginFragment {

    private static final String TAG = WriteCoverFragment.class.getSimpleName();
    private static final String VIEW_DATA = "view_data";

    private Controller mController;
    private WriteCoverViewData mData;

    private View mBtnNext;
    private EditText mSenderName;
    private EditText mSenderPhone;
    private EditText mReceiverName;
    private EditText mReceiverPhone;

    public static WriteCoverFragment createInstance(){
        WriteCoverFragment p = new WriteCoverFragment();
        p.setController(new WriteCoverController());
        return p;
    }

    public void setController(Controller controller){
        mController = controller;
    }

    public void setData(WriteCoverViewData data){
        mData = data;
    }

    public WriteCoverFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initSaveViewState(Bundle savedInstanceState) {
        if(savedInstanceState != null){
            if(mData == null)
                mData = WriteCoverViewData.build();
            mData.set(savedInstanceState.getBundle(VIEW_DATA));
        }
    }

    @Override
    protected void initToolbar(View v) {
//        Toolbar toolbar = (Toolbar) v.findViewById(R.id.app_toolbar).findViewById(R.id.awesome_toolbar);
//        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
    }

    @Override
    protected void init(View v) {
        mBtnNext = v.findViewById(R.id.wr_cover_btn_next);
        View senderForm = v.findViewById(R.id.wr_cover_cl_form_sender);
        View receiverForm = v.findViewById(R.id.wr_cover_cl_form_receiver);
        mSenderName = (EditText) senderForm.findViewById(R.id.sender_name);
        mSenderPhone = (EditText) senderForm.findViewById(R.id.sender_phone);
        mReceiverName = (EditText) receiverForm.findViewById(R.id.recv_name);
        mReceiverPhone = (EditText) receiverForm.findViewById(R.id.recv_phone);

        displayData();

        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mController != null) {

                    if(isReadyData()) {
                        mData = WriteCoverViewData.build()
                                .senderName(mSenderName.getText().toString())
                                .senderPhone(mSenderPhone.getText().toString())
                                .receiverName(mReceiverName.getText().toString())
                                .receiverPhone(mReceiverPhone.getText().toString());

                        mController.onNextClick(view, mData);
                    }
                    else{
                        Toast.makeText(getActivity(),
                                "Please enter Sender name and Receiver name",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void displayData() {
        if(mData != null){
            mSenderName.setText(mData.getSenderName());
            mSenderPhone.setText(mData.getSenderPhone());
            mReceiverName.setText(mData.getReceiverName());
            mReceiverPhone.setText(mData.getReceiverPhone());
        }
    }

    private boolean isReadyData() {
        return !mSenderName.getText().toString().equals("") && !mReceiverName.getText().toString().equals("");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(mData != null){
            outState.putBundle(VIEW_DATA, mData.get());
        }
        else{
            if(isReadyData()){
                mData = WriteCoverViewData.build()
                        .senderName(mSenderName.getText().toString())
                        .senderPhone(mSenderPhone.getText().toString())
                        .receiverName(mReceiverName.getText().toString())
                        .receiverPhone(mReceiverPhone.getText().toString());
                outState.putBundle(VIEW_DATA, mData.get());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Init Save state
        initSaveViewState(savedInstanceState);

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_write_cover, container, false);
        initToolbar(v);
        init(v);
        return v;
    }

    public interface Controller{
        void onNextClick(View sender, WriteCoverViewData data);
    }

}
